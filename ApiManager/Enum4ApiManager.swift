//
//  Enum4ApiManager.swift
//  ZiApp
//
//  Created by Carlos Méndez on 20/09/21.
//

import Foundation

//http://ziweb6c646c5223.codium.mx/api/facebookclickc8d683ecb8/10138/2?fromMobile=1

enum typeTap {
    case facebook
    case whatsapp
    case call
    case web
    case instagram
    case banner
    case recentlyViewed
    case recentlyAdded
    case all
}

struct MainUrl {
    static let production = "http://www.zientrevecinos.com/api"
    static let development = "http://ziweb6c646c5223.codium.mx/api"
}

struct TypeInsert {
    
    var adId = ""
    var userId = ""
    
    var facebook = ""
    var whatsapp = ""
    var call = ""
    var web = ""
    var instagram = ""
    
    var banner = ""
    var recentlyViewed = ""
    var recentlyAdded = ""
    var all = ""
    
    var type: typeTap
    
    init(adId: String = "", userId: String = "", typeSocialClic: typeTap) {
        self.adId = adId
        self.userId = userId
        self.type = typeSocialClic
        
        self.facebook = "/facebookclickc8d683ecb8/\(adId)/\(userId)?fromMobile=1"
        self.whatsapp = "/whatsappclick32bf6b180b/\(adId)/\(userId)?fromMobile=1"
        self.call = "/llamadaclicke9a96a0da5/\(adId)/\(userId)?fromMobile=1"
        self.web = "/paginawebclick9c1c35b8a7/\(adId)/\(userId)?fromMobile=1"
        self.instagram = "/instagramclick3db9a13278/\(adId)/\(userId)?fromMobile=1"
        
        self.banner = "/vistosrecientemente2e08a07c7e/\(adId)/\(userId)/1?fromMobile=1"
        self.recentlyViewed = "/vistosrecientemente2e08a07c7e/\(adId)/\(userId)/2?fromMobile=1"
        self.recentlyAdded = "/vistosrecientemente2e08a07c7e/\(adId)/\(userId)/3?fromMobile=1"
        self.all = "/vistosrecientemente2e08a07c7e/\(adId)/\(userId)/4?fromMobile=1"
    }
    
    func returnUrl() -> String {
        switch type {
        case .call:
            return self.call
        case .facebook:
            return self.facebook
        case .instagram:
            return self.instagram
        case .web:
            return self.web
        case .whatsapp:
            return self.whatsapp
        case .banner:
            return self.banner
        case .recentlyViewed:
            return self.recentlyViewed
        case .recentlyAdded:
            return self.recentlyAdded
        case .all:
            return self.all
        }
    }
}
