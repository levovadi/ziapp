import Foundation
import UIKit
import CryptoSwift

let urlLogin = "http://ziweb6c646c5223.codium.mx/api/loginusuarios605830aa19/oscar.fandino@palmersa.com/40bd001563085fc35165329ea1ff5c5ecbdbbeef/0/0/-/-?fromMobile=1"

struct SaludoResponse: Decodable {
    var flnombre504267dd91:String
    var flsaludoinicio801ebafd9e:String
}

struct LoginResponse: Decodable {
    var flcontrasenaa8f7f67349: String?
    var flcorreoelectronico57997cc6aa: String?
    var flidentificadore7dbf413f9: Int?
    var flzona621bfe9801: Int?
}

struct WelcomeResponse: Decodable {
    var fldescripcion639c11e75c: String?
    var flidentificador62fcc6ea76: Int?
    var flimagenview3a83e2f814: String?
    var fltitulod6250a7b31: String?
}

struct CategorieResponse: Decodable {
    var flcategoriadec9dce263: String?
    var flidentificadora4b8825c75: Int?
    var flimagenpreviewfa8e2604ad: String?
}

// it's the same for all
struct RecentAddedResponse: Decodable {
    var flidentificador9a5668476f: Int?
    var flimagenpreview67d7dcb6a0: String?
    var fltitulo83e9a8f656: String?
    var flwhatsappd3bc471ced: String?
}

struct FavoriteResponse: Decodable {
    var flanuncio4c84a1d494: Int?
    var fldescripcion7f0f0d9bc7: String?
    var flidentificador9a5668476f: Int?
    var flidentificadorb25af004f7: Int?
    var flimagenpreview67d7dcb6a0: String?
    var fltitulo83e9a8f656: String?
    var flusuarioc7f8e289bd: Int?
    var flwhatsappd3bc471ced: String?
}

struct DetailAdResponse: Decodable {
    var fldescripcion7f0f0d9bc7: String?
    var flfacebooke7b82defaa: String?
    var flidentificador9a5668476f: Int?
    var flimagenpreview67d7dcb6a0: String?
    var flinstagram8b80438c8d: String?
    var flpaginawebd22c4b4c53: String?
    var fltitulo83e9a8f656: String?
    var flwhatsappd3bc471ced: String?
    var flwhatsappgo9b768cb25f: String?
    var flwhatsapp5227d3664b73: String?
}

struct InfoAnunciosResponse: Decodable {
    var fldomingoa8273f920f2: String?
    var fldomingode45aefc131d: String?
    var flenviosa04ab1bf3a: Int
    var flfactura1621874f9b: Int?
    var flhorariosf4bea6d027: String?
    var fljuevesad6e0ac5f12: String?
    var fljuevesde4074e90d99: String?
    var fllunesa7be5f9030a: String?
    var fllunesde9e99afbcb7: String?
    var flmartesa91f1957abc: String?
    var flmartesdec5fbff10df: String?
    var flmiercolesa02dcd0d36a: String?
    var flmiercolesde7c98272aad: String?
    var flsabadoa23da90d617: String?
    var flsabadode15616d416d: String?
    var fltarjetadecredito73c161d82e: Int?
    var flviernesab7f6689767: String?
    var flviernesde5fee266186:String?
}

struct EstrellaCalificacionResponse: Decodable {
    var flidentificador9a5668476f: Int?
    var flpromediob2b428e469: String?
}


struct ZonaResponse: Decodable {
    var fleliminarzonaprocedimiento91d6c0ebc6: String?
    var fleliminarzonasubconsulta0c613f85da: String?
    var fliconoeliminarzonab9c7898cf9: String?
    var flidentificador6d8056256d: Int?
    var flzona0119225338: String?
}

struct SummaryResponse: Decodable {
    var flconteo62a57e05ef: String?
    var flidentificador9a5668476f: Int?
    var flpromediob2b428e469 : String?
}

struct CommentResponse: Decodable {
    var flcalificacion3fe0b68f3d: Int?
    var flcomentariob330b9557e: String?
    var flfecha66152f75a3: String?
    var flformatodefecha9081060b72: String?
}

struct searchResponse: Decodable {
    var fldescripcion7f0f0d9bc7: String?
    var flidentificador9a5668476f: Int?
    var flimagenpreview67d7dcb6a0: String?
    var fltitulo83e9a8f656: String?
    var flwhatsappd3bc471ced: String?
}

struct TitlesSectionResponse: Decodable {
    var flcategoriadec9dce263: String?
    var flcategoriaspara90c8d88493: String?
    var flpromocionesparafc4293c5f6: String?
    var fltodoslosanunciospara6e6353758f: String?
}

struct PromotionResponse: Decodable {
    var flanuncioa653fd3644: Int?
    var flidentificador298abc5079: Int?
    var flimagenviewebbb866cf0: String?
    var fltitulopromocionbe71cf67ac: String?
}


struct ProfileResponse: Decodable {
    var flcorreoelectronico57997cc6aa: String?
    var flidentificadore7dbf413f9: Int?
    var flnombre504267dd91: String?
    var flzona621bfe9801: Int?
}

struct SecondaryImagesResponse: Decodable {
    var flidentificador00efe14b5c: Int?
    var flimagenpreview3e0731de4e: String?
}

struct subCategoryResponse: Decodable {
    var flidentificadorb9c0b0d58c: Int?
    var flsubcategoria4d0825099c: String?
}

struct subCategoryDetailResponse: Decodable {
    var flidentificadorb9c0b0d58c: Int
    var flimagenpreview1a619ca85d: String?
    var flsubcategoria4d0825099c: String?
}

struct BannerResponse: Decodable {
    var flanunciofedb145059: Int?
    var flestatus55c8c29f67: Int?
    var flidentificador9a5668476f: Int?
    var flidentificador5f1d1f1623: Int?
    var flmagenviewdb31471f8f: String?
    var fltitulobanner8be927964a: String?
    var flwhatsappd3bc471ced: String?
}

struct VistosRecientementeResponse: Decodable {
    var flidentificador9a5668476f: Int?
    var flimagenpreview67d7dcb6a0: String?
    var flwhatsappd3bc471ced: String?
}

struct RegisterResponse: Decodable {
    var flcontrasenaa8f7f67349: String?
    var flcorreoelectronico57997cc6aa: String?
    var fldirecciona44ef80103: String?
    var flidentificadore7dbf413f9: Int?
    var flnombre504267dd91: String?
    var flnumerof8372095b4: String?
    var flreferenciasdomicilio3a9277e111: String?
    var flzona621bfe9801: Int?
    var flzona9bdc0868f7: Int?
}


class ApiManager {
    
    var semaphore = DispatchSemaphore(value: 0)
    public static let shared = ApiManager()
    
    func loginWs(usermail: String , password: String , isLogged: Bool? = false ) -> [LoginResponse] {
        var result: [LoginResponse] = []
        var urlString = ""
        if isLogged ?? false {
            urlString = "\(baseUrl)/api/loginusuarios605830aa19/\(usermail.lowercased())/\(password)/0/0/-/-?fromMobile=1"
        } else {
            urlString = "\(baseUrl)/api/loginusuarios605830aa19/\(usermail.lowercased())/\(password.sha1())/0/0/-/-?fromMobile=1"
        }
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let user = try JSONDecoder().decode([LoginResponse].self, from: data ?? Data())
                    DataManager.shared.saveUserData(userData: user.first ?? LoginResponse())
                    result = user
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func welcomeWs() -> [WelcomeResponse] {
        var result: [WelcomeResponse] = []
        let urlString = "\(baseUrl)/api/anunciosadminapp2c93111d1c/0/0/-/-?fromMobile=1"
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let user = try JSONDecoder().decode([WelcomeResponse].self, from: data ?? Data())
                    result = user
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func saludoInicial(identificador: Int ) -> [SaludoResponse] {
        let urlString = "\(baseUrl)/api/saludoinicioranab25a9394f1/\(identificador)/0/0/-/-?fromMobile=1"
        var result: [SaludoResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let user = try JSONDecoder().decode([SaludoResponse].self, from: data ?? Data())
                    result = user
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func categoriesWs() -> [CategorieResponse] {
        let urlString = "\(baseUrl)/api/categoriasdf93ab5729/0/0/-/-?fromMobile=1"
        var result: [CategorieResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let user = try JSONDecoder().decode([CategorieResponse].self, from: data ?? Data())
                    result = user
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    // Agregados recientemente categoria
    // /api/vistosrecientemente2e08a07c7e/
    // /api/vistosrecientemente2e08a07c7e/{paranunciosd55b0311bfCRUD_0}/{parusuariod99f65acabCRUD_0
    
    // /api/mdanunciostotalesadmin51d2b375f4/
    ///api/mdanunciostotalesadmin51d2b375f4/{par4309d4fa6803449d98932f1c826aa4c9}/{page}/{pageSize}/{orderColumn}/{orderType}
    
    func recentlyAddedWs(category: Int, zonaId:Int ) -> [RecentAddedResponse] {
        let urlString = "\(baseUrl)/api/agregadosrecientementeffc35ce83c/\(category)/\(zonaId)/0/0/-/-?fromMobile=1"
        var result: [RecentAddedResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let user = try JSONDecoder().decode([RecentAddedResponse].self, from: data ?? Data())
                    result = user
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }

    func vistosRecientemente(profileId: Int,category: Int,zoneId: Int) -> [VistosRecientementeResponse] {
        let urlString = "\(baseUrl)/api/vistosrecientementeapp8a9c2c77ec/\(profileId)/\(category)/\(zoneId)/0/0/-/-?fromMobile=1"
        var result: [VistosRecientementeResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let anuncios = try JSONDecoder().decode([VistosRecientementeResponse].self, from: data ?? Data())
                    result = anuncios
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getAllAnuncios(idCategory: Int,idZona: Int) -> [RecentAddedResponse] {
        let urlString = "\(baseUrl)/api/anuncios724acc486c/\(idCategory)/-/\(idZona)/0/0/-/-?fromMobile=1"
        var result: [RecentAddedResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let anuncios = try JSONDecoder().decode([RecentAddedResponse].self, from: data ?? Data())
                    result = anuncios
                    self.semaphore.signal()
                } catch _ as NSError {
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func favoritesWs(idUser: Int) -> [FavoriteResponse] {
        let urlString = baseUrl + "/api/favoritos1d2d39e291/\(idUser)/-/-/0/0/-/-?fromMobile=1"
        var result: [FavoriteResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ categoriesWs Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let favorite = try JSONDecoder().decode([FavoriteResponse].self, from: data ?? Data())
                    result = favorite
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getAdDetailWs(identificador: Int) -> [DetailAdResponse] {
        let urlString = "\(baseUrl)/api/anunciodetalle067081f888/\(identificador)/0/0/-/-?fromMobile=1"
        var result: [DetailAdResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let favorite = try JSONDecoder().decode([DetailAdResponse].self, from: data ?? Data())
                    result = favorite
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getinfoanuncioWs(id: Int) -> [InfoAnunciosResponse] {
        let urlString = "\(baseUrl)/api/infoanuncio8faeb182c5/\(id)/0/0/-/-?fromMobile=1"
        var result: [InfoAnunciosResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ InfoAnunciosResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let favorite = try JSONDecoder().decode([InfoAnunciosResponse].self, from: data ?? Data())
                    result = favorite
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getStarRatingWs(id: Int) -> [EstrellaCalificacionResponse] {
        let urlString = "\(baseUrl)/api/estrellacalificacion9c95c88fe1/\(id)/0/0/-/-?fromMobile=1"
        var result: [EstrellaCalificacionResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ EstrellaCalificacionResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let favorite = try JSONDecoder().decode([EstrellaCalificacionResponse].self, from: data ?? Data())
                    result = favorite
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getZonasWs() -> [ZonaResponse] {
        let urlString = "\(baseUrl)/api/mdzonas93d0e92627/0/0/-/-?fromMobile=1"
        var result: [ZonaResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ ZonaResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let zonas = try JSONDecoder().decode([ZonaResponse].self, from: data ?? Data())
                    result = zonas
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func summaryRatingAndCommentsWs(identificador: Int) -> [SummaryResponse] {
        let urlString = "\(baseUrl)/api/calificacionesanuncio6f73f5ae04/\(identificador)/0/0/-/-?fromMobile=1"
        var result: [SummaryResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ SummaryResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let summary = try JSONDecoder().decode([SummaryResponse].self, from: data ?? Data())
                    result = summary
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getCommetsWs(id: Int) -> [CommentResponse] {
        let urlString = "\(baseUrl)/api/recuperarcomentarios973e96d625/\(id)/0/0/-/-?fromMobile=1"
        var result: [CommentResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ SummaryResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let comments = try JSONDecoder().decode([CommentResponse].self, from: data ?? Data())
                    result = comments
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func searchqueryWs(zonaId: Int ,word: String) -> [searchResponse] {
        let urlString = "\(baseUrl)/api/busquedaanuncios1cb45cd557/\(zonaId)/\(word)/\(word)/0/0/-/-?fromMobile=1"
        var result: [searchResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ SummaryResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let search = try JSONDecoder().decode([searchResponse].self, from: data ?? Data())
                    result = search
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getTitlesSections(id: Int) -> [TitlesSectionResponse] {
        let urlString = "\(baseUrl)/api/mundorecuperar220095a656/\(id)/0/0/-/-?fromMobile=1"
        var result: [TitlesSectionResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ TitlesSectionResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let search = try JSONDecoder().decode([TitlesSectionResponse].self, from: data ?? Data())
                    result = search
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getPromotiosByCategory(zoneId:Int , categoryId: Int) -> [PromotionResponse] {
        let urlString = "\(baseUrl)/api/promocionescfff8a2c65/\(categoryId)/\(zoneId)/-/-/0/0/-/-?fromMobile=1"
        var result: [PromotionResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ PromotionResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let promotions = try JSONDecoder().decode([PromotionResponse].self, from: data ?? Data())
                    result = promotions
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func addToFavorites(identificador: Int,profileId: Int) -> Int {
        var statusCodeResponse: Int = 0
        let urlString = "\(baseUrl)/api/favoritoa035d562af/\(identificador)/\(profileId)?fromMobile=1"
        guard let url = URL(string: urlString) else { return 0 }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                    let result = response as? HTTPURLResponse
                     let statusCode = result?.statusCode
                    statusCodeResponse = statusCode ?? 0
                    self.semaphore.signal()
            }
        })
        dataTask.resume()
        semaphore.wait()
        return statusCodeResponse
    }
    
    func removeFromFavorites(identificador: Int, profileIdentificador: Int) -> Int {
        var statusCodeResponse: Int = 0
        let urlString = "\(baseUrl)/api/quitarfavoritodfca4680c3//\(identificador)/\(profileIdentificador)?fromMobile=1"
        guard let url = URL(string: urlString) else { return 0 }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ PromotionResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                    let result = response as? HTTPURLResponse
                     let statusCode = result?.statusCode
                    statusCodeResponse = statusCode ?? 0
                    self.semaphore.signal()
            }
        })
        dataTask.resume()
        semaphore.wait()
        return statusCodeResponse
    }
    
    func getInfoCurrentProfile(id: Int) -> [ProfileResponse] {
        let urlString = "\(baseUrl)/api/usuariosrecuperar757b64d78b/\(id)/0/0/-/-?fromMobile=1"
        var result: [ProfileResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("[ ProfileResponse Datatask error ]",error?.localizedDescription ?? NSError())
                self.semaphore.signal()
            } else {
                do {
                    let profile = try JSONDecoder().decode([ProfileResponse].self, from: data ?? Data())
                    result = profile
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getSecundaryImages(id: Int) -> [SecondaryImagesResponse] {
        let urlString = "\(baseUrl)/api/imagenesanunciodetalle532fa9224f/\(id)/0/0/-/-?fromMobile=1"
        var result: [SecondaryImagesResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let profile = try JSONDecoder().decode([SecondaryImagesResponse].self, from: data ?? Data())
                    result = profile
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getSubcategories(identificador: Int) -> [subCategoryResponse] {
        let urlString = "\(baseUrl)/api/subcategoriasb44144f14b/\(identificador)/-/0/0/-/-?fromMobile=1"
        var result: [subCategoryResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let subcategories = try JSONDecoder().decode([subCategoryResponse].self, from: data ?? Data())
                    result = subcategories
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
   
    func getDetailSubcategory(idSubcategory: Int) -> [subCategoryDetailResponse] {
        let urlString = "\(baseUrl)/api/mdvercategoriasaace91e485/\(idSubcategory)/0/0/-/-?fromMobile=1"
        var result: [subCategoryDetailResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let subcategories = try JSONDecoder().decode([subCategoryDetailResponse].self, from: data ?? Data())
                    result = subcategories
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func getBannerWs(idCategory: Int,idZone: Int) -> [BannerResponse] {
        let urlString = "\(baseUrl)/api/bannerpromocionalb3075688f8/\(idCategory)/\(idZone)/0/0/-/-?fromMobile=1"
        var result: [BannerResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let banners = try JSONDecoder().decode([BannerResponse].self, from: data ?? Data())
                    result = banners
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    if error.localizedDescription == "Failed to load: The data couldn’t be read because it isn’t in the correct format." {
                        print("[Si estas usando Decodable revisa el modelo corresponda al response]")
                    }
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func searchAnuncioBySubcategory(idCategory: Int,
                                    idSubcategory: Int) -> [RecentAddedResponse] {
        let urlString = "\(baseUrl)/api/anuncios724acc486c/\(idCategory)/\(idSubcategory)/1/0/0/-/-?fromMobile=1"
        var result: [RecentAddedResponse] = []
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let subcategories = try JSONDecoder().decode([RecentAddedResponse].self, from: data ?? Data())
                    result = subcategories
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        })
        dataTask.resume()
        semaphore.wait()
        return result
    }
    
    func AddToRecentlyViewed(AdId: Int, UserId: Int) -> Int {
        var statusCode = 0
        let urlString = "\(baseUrl)/api/vistosrecientemente2e08a07c7e/\(AdId)/\(UserId)/?fromMobile=1"
        guard let url = URL(string: urlString) else { return statusCode }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.semaphore.signal()
            } else {
                let responseDecode = response as? HTTPURLResponse
                statusCode = responseDecode?.statusCode ?? 0
                self.semaphore.signal()
            }
        }).resume()
        
        semaphore.wait()
        return statusCode
    }
    
    // http://ziweb6c646c5223.codium.mx/external/registro780a2ce9fc_inscribirse9b3d5c71b9?fromMobile=1
    
    func registerWs(name: String , mail: String ,pwd: String ,zone: Int) -> [RegisterResponse] {
        var result: [RegisterResponse] = []
        let urlString = "\(baseUrl)/external/registro780a2ce9fc_inscribirse9b3d5c71b9?fromMobile=1"
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.timeoutInterval = 60
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        var requestBodyComponents = URLComponents()
        requestBodyComponents.queryItems = [ URLQueryItem(name: "ctlcontrasena09758c3d9a_registro780a2ce9fc", value: "\(pwd)"),
                                             URLQueryItem(name: "ctlemail9559e885fb_registro780a2ce9fc", value: "\(mail)"),
                                             URLQueryItem(name: "ctlnombrea3f3319b7f_registro780a2ce9fc", value: "\(name)"),
                                             URLQueryItem(name: "ctlzonae379e8da86_registro780a2ce9fc", value: "\(zone)"),
                                             URLQueryItem(name: "ctlvariabledesesion8de752494b_miperfil8f8d155c90", value: "0"),
                                             URLQueryItem(name: "hidregistro780a2ce9fc", value: "registro780a2ce9fc"),
                                             URLQueryItem(name: "ctlguardar96e57967e5", value: "1"),
        ]
        request.httpBody = requestBodyComponents.query?.data(using: .utf8)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
            if (err != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let registeruser = try JSONDecoder().decode([RegisterResponse].self, from: data ?? Data())
                    result = registeruser
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        }.resume()
        semaphore.wait()
        return result
    }
    
    func updateProfileData(name: String , mail: String ,pwd: String ,zone: Int,sessionId: Int) -> [RegisterResponse] {
        var result: [RegisterResponse] = []
        let urlString = "\(baseUrl)/external/miperfil8f8d155c90_miperfil3802d18ad6?fromMobile=1"
        guard let url = URL(string: urlString) else { return result }
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.timeoutInterval = 60
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        var requestBodyComponents = URLComponents()
        requestBodyComponents.queryItems = [ URLQueryItem(name: "ctlemail9559e885fb_miperfil8f8d155c90", value: "\(mail)"),
                                             URLQueryItem(name: "ctlnombrea3f3319b7f_miperfil8f8d155c90", value: "\(name)"),
                                             URLQueryItem(name: "ctlzonae379e8da86_miperfil8f8d155c90", value: "\(zone)"),
                                             URLQueryItem(name: "ctlcontrasena09758c3d9a_miperfil8f8d155c90", value: "\(pwd)"),
                                             URLQueryItem(name: "ctlvariabledesesion8de752494b_miperfil8f8d155c90", value: "\(sessionId)"),
                                             URLQueryItem(name: "hidmiperfil8f8d155c90", value: "miperfil8f8d155c90"),
                                             URLQueryItem(name: "ctlguardar96e57967e5", value: "1"),
        ]
    
        
        request.httpBody = requestBodyComponents.query?.data(using: .utf8)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
            if (err != nil) {
                self.semaphore.signal()
            } else {
                do {
                    let registeruser = try JSONDecoder().decode([RegisterResponse].self, from: data ?? Data())
                    result = registeruser
                    self.semaphore.signal()
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                    self.semaphore.signal()
                }
            }
        }.resume()
        semaphore.wait()
        return result
    }
    
    func insertClic(adId: String, userId: String , type: typeTap) -> Int {
        var statusCode: Int = 0
        let urlString = "\(MainUrl.production)\(TypeInsert.init(adId: adId, userId: userId, typeSocialClic: type).returnUrl())"
        print(urlString)
        guard let url = URL(string: urlString) else { return statusCode }
        print("Consulta esta url \(url)")
        let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                debugPrint(error)
                self.semaphore.signal()
            } else {
                let responseDecode = response as? HTTPURLResponse
                statusCode = responseDecode?.statusCode ?? 0
                print(statusCode)
                self.semaphore.signal()
            }
        }).resume()
        semaphore.wait()
        return statusCode
    }
}
