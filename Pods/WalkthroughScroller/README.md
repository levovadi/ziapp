# Walkthrough Scroller
This project allows you to create a walkthrough animation in iOS apps.

<img src="https://github.com/sanchgoel/WalkthroughScroller/blob/master/Video/walkthroughScroller.gif" width="300" />
