//
//  LoginUITest.swift
//  ZiAppUITests
//
//  Created by Carlos Méndez on 07/05/21.
//

import XCTest


class LoginUITest: XCTestCase {
    
    
    override class func setUp() {

    }

    override func setUpWithError() throws {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    override func tearDownWithError() throws {
        
    }
    
    func testApp() {
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
        let invitedButton = app.buttons["INVITADO"]
        let exists = NSPredicate(format: "exists == 1")
        expectation(for: exists, evaluatedWith: invitedButton, handler: nil)
        waitForExpectations(timeout: 30, handler: nil)
        invitedButton.tap()
        let confirmButtonZone = app.buttons["guardar"]
        let existsConfirm = NSPredicate(format: "exists == 1")
        expectation(for: existsConfirm, evaluatedWith: confirmButtonZone, handler: nil)
        waitForExpectations(timeout: 30, handler: nil)
        confirmButtonZone.tap()
        let elementsQuery = app.alerts.firstMatch
        elementsQuery.buttons["Aceptar"].tap()
        let collectionWorlds = app.collectionViews.firstMatch
        snapshot("MenuMundos")
        let cellWorld = collectionWorlds.cells.firstMatch
        cellWorld.tap()
        snapshot("BottomMenu")
    }

}
