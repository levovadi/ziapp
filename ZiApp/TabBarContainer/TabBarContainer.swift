import Foundation
import UIKit

class TabBarContainerController: UITabBarController, UITabBarControllerDelegate {
    
    var arrayBottomTabImages: [String] = ["img10932","img10934","img10936","img10938","img10940"]
    var arraySelectedTabImages: [String] = ["img10933","img10935","img10937","img10939","img10941"]
    var idCategorySelected: Int = -1
    
    lazy var indicatorBottomCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(ImageCategoryCell.self, forCellWithReuseIdentifier: ImageCategoryCell.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        listOptions.tag = 1
        return listOptions
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        configureTabBarController()
        populateDataControllers()
    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubviews()
        setAutolayout()
        configCollection()
        configureTabBarController()
    }
    
    func populateDataControllers() {
        self.delegate = self
        let currentUser = DataManager.shared.getDataUser()
        let first = self.viewControllers?.first as? DetailScreenController
        first?.categoryId = idCategorySelected
        first?.converToDictionary(identificador: idCategorySelected)
        let second = self.viewControllers?[2] as? PromotionsVC
        second?.setTitleCategory(catId: idCategorySelected)
        second?.promotions = ApiManager.shared.getPromotiosByCategory(zoneId: currentUser.flzona621bfe9801 ?? 0, categoryId: idCategorySelected)
        if let controllerCategories = self.viewControllers?[2] as? SubcategoriesController {
            controllerCategories.setTitleCategory(catId: idCategorySelected)
            controllerCategories.parentCategory = idCategorySelected
            controllerCategories.configureData()
        }
    }
    
    func setSubviews() {
        self.view.addSubview(indicatorBottomCollection)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            indicatorBottomCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            indicatorBottomCollection.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0),
            indicatorBottomCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.95),
            indicatorBottomCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15)
        ])
    }
    
    func configCollection() {
        self.indicatorBottomCollection.delegate = self
        self.indicatorBottomCollection.dataSource = self
    }
    
    func configureTabBarController() {
        let firstController = DetailScreenController()
        firstController.tabBarItem.title = "detail"
        firstController.tabBarItem.tag = 0
        let secondController = FavoritesController()
        secondController.tabBarItem.title = "second"
        secondController.tabBarItem.tag = 1
        let thirdController = PromotionsVC()
        secondController.tabBarItem.title = "third"
        secondController.tabBarItem.tag = 2
        let fourController = SubcategoriesController()
        fourController.tabBarItem.title = "four"
        fourController.tabBarItem.tag = 3
        let fiveController = HelpScreen()
        fourController.tabBarItem.title = "five"
        fourController.tabBarItem.tag = 4
        self.setViewControllers([firstController,secondController,thirdController,fourController,fiveController], animated: true)
        self.delegate = self
        self.tabBar.isHidden = true
    }
    
    
    
}

extension TabBarContainerController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayBottomTabImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  ImageCategoryCell.id, for: indexPath) as! ImageCategoryCell
        cell.backgroundColor = .clear
        cell.imageCatalog.backgroundColor = .clear
        cell.imageCatalog.image = UIImage(named: arrayBottomTabImages[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ImageCategoryCell {
            cell.backgroundColor = .clear
            cell.imageCatalog.backgroundColor = .clear
            cell.imageCatalog.image = UIImage(named: arraySelectedTabImages[indexPath.row])
            self.selectedIndex = indexPath.item
            self.tabBar(self.tabBar, didSelect: self.viewControllers?[self.selectedIndex].tabBarItem ?? UITabBarItem())
            self.tabBarController(self.tabBarController ?? UITabBarController(), didSelect: self.viewControllers?[self.selectedIndex] ?? UIViewController())
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ImageCategoryCell {
            cell.backgroundColor = .clear
            cell.imageCatalog.backgroundColor = .clear
            cell.imageCatalog.image = UIImage(named: arrayBottomTabImages[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.screenType.rawValue.contains("XR") || UIDevice.current.screenType.rawValue.contains("X") || UIDevice.current.screenType.rawValue.contains("XS") || UIDevice.current.screenType.rawValue.contains("Mini") || UIDevice.current.screenType.rawValue.contains("12") || UIDevice.current.screenType.rawValue.contains("11")  {
            let wCell = view.frame.height * 0.08
            let hCell = view.frame.height * 0.08
            return CGSize(width: wCell, height: hCell)
        }
        
        let wCell = view.frame.height * 0.10
        let hCell = view.frame.height * 0.10
        return CGSize(width: wCell, height: hCell)
    }
    
}

extension TabBarContainerController {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "inicio" {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let controllerCategories = viewController as? SubcategoriesController {
            controllerCategories.setTitleCategory(catId: idCategorySelected)
            controllerCategories.parentCategory = idCategorySelected
            DataManager.shared.saveCurrentCategory(idCat: idCategorySelected)
            controllerCategories.configureData()
        }
    }
    
}

