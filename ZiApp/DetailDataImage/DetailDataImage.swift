import Foundation
import UIKit
import ImageSlideshow
import Cosmos
import Kingfisher
import SafariServices
import AlamofireImage
import Alamofire
import WebKit

class DetailDataImage: UIViewController {
    var currentCategoryId = 1
    var flag = true
    let localSource = [BundleImageSource(imageString: "img1"), BundleImageSource(imageString: "img2"), BundleImageSource(imageString: "img3"), BundleImageSource(imageString: "img4")]
    lazy var searchBar = UISearchBar()
    var objectForLinks: DetailAdResponse?
    var resourcesPath: [String] = []
    var resultados: [KingfisherSource] = []
    var hidebar: Bool = false
    weak var delegate: reloadVistosRecientemente?
    var arrayBottomTabImages: [String] = ["img10932","img10934","img10936","img10938","img10940"]
    var arraySelectedTabImages: [String] = ["img10933","img10935","img10937","img10939","img10941"]
    
    lazy var indicatorBottomCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(ImageCategoryCell.self, forCellWithReuseIdentifier: ImageCategoryCell.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        listOptions.tag = 1
        listOptions.isHidden = true
        return listOptions
    }()
    
    lazy var ratintView: CosmosView = {
        let properties = CosmosView(frame: .zero)
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.rating = 0.0
        properties.settings.fillMode = .half
        properties.isUserInteractionEnabled = false
        properties.settings.disablePanGestures = true
        return properties
    }()
    
    lazy var scroll: UIScrollView = {
        let config = UIScrollView()
        config.translatesAutoresizingMaskIntoConstraints = false
        config.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        return config
    }()
    
    lazy var frameVideo: WKWebView = {
        let properties = WKWebView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .clear
        return properties
    }()
    
    var hereText: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Pide aquí"
        properties.textColor = UIColor.black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 22)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var countComentaries: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "0 comentarios"
        properties.textColor = UIColor.black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 22)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var favoriteLB: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Guardar como favorito"
        properties.textColor = .black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 17)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var imageFavorite: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "empty")
        properties.contentMode = .scaleAspectFit
        properties.isUserInteractionEnabled = true
        properties.backgroundColor = .clear
        return properties
    }()
    
    var contentView: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .clear
        return properties
    }()
    
    var contentViewYellow: UIView = {
        let properties = UIView()
        properties.backgroundColor = .yellow
        properties.translatesAutoresizingMaskIntoConstraints = false
        return properties
    }()
    
    var titleLB: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Aqui va el texto del titulo o lo que sea"
        properties.textColor = primaryColorGreen
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 20)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var imagePrincipal: ImageSlideshow = {
        let properties = ImageSlideshow()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.slideshowInterval = 5.0
        properties.contentScaleMode = .scaleAspectFit
        properties.pageIndicator = UIPageControl.withSlideshowColors()
        properties.activityIndicator = DefaultActivityIndicator()
        return properties
    }()
    
    var imageWeb: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "web")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var imageWhats: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "whats")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var imageFacebook: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "facebook")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var imageInstagram: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "instagram")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var callIcon: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "callIcon")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var ratingIcon: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "rating")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var basicInformationIcon: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "basicInformation")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var stackMedia0 : UIStackView = {
        let properties = UIStackView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.distribution = .fillEqually
        properties.axis = .horizontal
        return properties
    }()
    
    var stackMedia1 : UIStackView = {
        let properties = UIStackView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.distribution = .fillEqually
        properties.axis = .horizontal
        return properties
    }()
    
    var slideshow: ImageSlideshow = {
        let properties = ImageSlideshow()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.slideshowInterval = 5.0
        properties.contentScaleMode = .scaleAspectFit
        properties.pageIndicator = UIPageControl.withSlideshowColors()
        properties.activityIndicator = DefaultActivityIndicator()
        return properties
    }()
    
    lazy var descriptionLabel: UILabel = {
        let prop = UILabel()
        prop.translatesAutoresizingMaskIntoConstraints = false
        prop.backgroundColor = .clear
        prop.text = ""
        prop.numberOfLines = 0
        prop.textAlignment = .center
        prop.textColor = .black
        prop.sizeToFit()
        return prop
    }()
    
    override func viewDidDisappear(_ animated: Bool) {
        let datos = ["categoryId": "\(currentCategoryId)"]
        delegate?.reloadData(with: datos)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.titleView?.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        setAutolayout()
        otherOptionSlideShow()
        ratintView.didFinishTouchingCosmos = { rating in}
        ratintView.didTouchCosmos = { rating in}
        let gestureFavorite = UITapGestureRecognizer(target: self, action: #selector(addFavorite(tapGestureRecognizer:)))
        self.imageFavorite.addGestureRecognizer(gestureFavorite)
        searchBar.placeholder = "Buscador"
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
        setNavItems()
        tabmenuCollectionConfiguration()
    }
    
    func tabmenuCollectionConfiguration() {
        indicatorBottomCollection.delegate = self
        indicatorBottomCollection.dataSource = self
    }
    
    func setNavItems() {
        let searchIcon = UIImage(named: "3844432-24")
        let searchItem = UIBarButtonItem(image: searchIcon?.withRenderingMode(.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(searchAction))
        searchItem.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = searchItem
    }
    
    @objc func searchAction() {
        if !hidebar {
            self.navigationItem.titleView?.isHidden = false
        } else {
            self.navigationItem.titleView?.isHidden = true
        }
        hidebar = !hidebar
    }
    
    @objc func addFavorite(tapGestureRecognizer: UITapGestureRecognizer) {
        if !flag {
            removeFromFavorite(identificador: objectForLinks?.flidentificador9a5668476f ?? 0)
        } else {
            let currentUser = DataManager.shared.getDataUser()
            let favoriteFlag = ApiManager.shared.addToFavorites(identificador: objectForLinks?.flidentificador9a5668476f ?? 0, profileId: currentUser.flidentificadore7dbf413f9 ?? 0)
            if favoriteFlag == 204 {
                self.present(AlertManager.shared.defaultalert(title: "", message: "Se agregó exitosamente a favoritos"), animated: true, completion: nil)
                self.imageFavorite.image = UIImage(named: "checked")
                flag = !flag
            } else {
                self.present(AlertManager.shared.defaultalert(title: "", message: "ocurrió un error al agregar a favoritos"), animated: true, completion: nil)
            }
        }
    }
    
    func removeFromFavorite(identificador: Int?) {
        let alerta = UIAlertController(title: "", message: "¿Estás seguro de borrar de favoritos?", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Aceptar", style: .default) { [self] (acc) in
            let currentUser = DataManager.shared.getDataUser()
            let rCode = ApiManager.shared.removeFromFavorites(identificador: identificador ?? 0, profileIdentificador: currentUser.flidentificadore7dbf413f9 ?? 0)
            if rCode == 204 {
                self.imageFavorite.image = UIImage(named: "empty")
                self.present(AlertManager.shared.defaultalert(title: "", message: "Se eliminó exitosamente de tus favoritos"), animated: true, completion: nil)
                flag = !flag
            } else {
                self.present(AlertManager.shared.defaultalert(title: "", message: "Ocurrió un error al agregar a favoritos"), animated: true, completion: nil)
            }
        }
        let action2 = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
        alerta.addAction(accion)
        alerta.addAction(action2)
        self.navigationController?.present(alerta, animated: true, completion: nil)
    }

    func addSubviews() {
        self.view.addSubview(scroll)
        self.scroll.addSubview(titleLB)
        self.scroll.addSubview(imagePrincipal)
        self.scroll.addSubview(hereText)
        stackMedia0.addArrangedSubview(imageWhats)
        stackMedia0.addArrangedSubview(imageWeb)
        stackMedia0.addArrangedSubview(imageFacebook)
        stackMedia0.addArrangedSubview(imageInstagram)
        self.scroll.addSubview(stackMedia0)
        self.scroll.addSubview(callIcon)
        self.scroll.addSubview(basicInformationIcon)
        self.scroll.addSubview(slideshow)
        self.scroll.addSubview(contentView)
        self.contentView.addSubview(favoriteLB)
        self.contentView.addSubview(imageFavorite)
        self.scroll.addSubview(ratintView)
        self.scroll.addSubview(countComentaries)
        self.scroll.addSubview(ratingIcon)
        self.scroll.addSubview(descriptionLabel)
        self.scroll.addSubview(frameVideo)
        self.view.addSubview(indicatorBottomCollection)
    }
    
    func setDetailData(detailData: [DetailAdResponse]) {
        let currentUser = DataManager.shared.getDataUser()
        self.addTargetsOrActionsToImage()
        DispatchQueue.main.async { [self] in
            guard let detail = detailData.first else { return }
            let _ = ApiManager.shared.AddToRecentlyViewed(AdId: detail.flidentificador9a5668476f ?? 0, UserId: currentUser.flidentificadore7dbf413f9 ?? 0)
            self.validateFavorite(idAd: detail.flidentificador9a5668476f ?? 0)
            self.objectForLinks = detail
            self.loadSecondaryImages(idToSearchSecondaryImages: detail.flidentificador9a5668476f ?? 0)
            self.titleLB.text = self.objectForLinks?.fltitulo83e9a8f656
            guard let description = detail.fldescripcion7f0f0d9bc7 else { return }
            self.descriptionLabel.text = "Descripción\n\n" + description
            guard let path = detail.flimagenpreview67d7dcb6a0 else { return }
            let completepath = baseUrl + path
            guard let url = URL(string: completepath) else { return }
            self.imagePrincipal.setImageInputs([KingfisherSource(url: url)])
            let promedio = ApiManager.shared.summaryRatingAndCommentsWs(identificador: detail.flidentificador9a5668476f ?? 0)
            guard let promedioObject = promedio.first else { return }
            if let promedioDouble = Double(promedioObject.flpromediob2b428e469 ?? "0") {
                self.ratintView.rating = promedioDouble
            }
            self.countComentaries.text = promedioObject.flconteo62a57e05ef
            self.loadFrame()
        }
    }
    
    // MARK: ISSUE VIDEO
    // guard let promedioDouble = Double(promedioObject.flpromediob2b428e469 ?? "0") else { return }
    
    func validateFavorite(idAd: Int) {
        let favoritesArray = ApiManager.shared.favoritesWs(idUser: DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)
        let filterAd = favoritesArray.filter({ $0.flidentificador9a5668476f ==  idAd})
        if filterAd.count > 0 {
            flag = false
            imageFavorite.image = UIImage(named: "checked")
        }
    }
    
    func loadSecondaryImages(idToSearchSecondaryImages: Int) {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailDataImage.didTap))
        slideshow.addGestureRecognizer(gestureRecognizer)
        let imagesSecondaries = ApiManager.shared.getSecundaryImages(id: idToSearchSecondaryImages)
        for resource in imagesSecondaries {
            resourcesPath.append(resource.flimagenpreview3e0731de4e ?? "")
        }
        for path in resourcesPath {
            let completepath = baseUrl + path
            guard let urlImage = URL(string: completepath) else { return }
            let item = KingfisherSource(url: urlImage)
            resultados.append(item)
        }
        self.slideshow.setImageInputs(resultados)
    }
    
    private func loadFrame() {
        let urlString = "\(baseUrl)/Video/\(objectForLinks?.flidentificador9a5668476f ?? 0)"
        self.frameVideo.navigationDelegate = self
        if let urlRequest = URL(string: urlString) {
            self.frameVideo.load(URLRequest(url: urlRequest))
        } else {
            self.frameVideo.isHidden = true
        }
    }
    
    @objc func didTap() {
      slideshow.presentFullScreenController(from: self)
    }
    
    @objc func didTapToZoomImage() {
      imagePrincipal.presentFullScreenController(from: self)
    }
    
    func addTargetsOrActionsToImage() {
        imageWeb.isUserInteractionEnabled = true
        imageWhats.isUserInteractionEnabled = true
        imageFacebook.isUserInteractionEnabled = true
        imageInstagram.isUserInteractionEnabled = true
        basicInformationIcon.isUserInteractionEnabled = true
        ratingIcon.isUserInteractionEnabled = true
        callIcon.isUserInteractionEnabled = true
    
        
        let gesture0 = UITapGestureRecognizer(target: self, action: #selector(changeOption0(tapGestureRecognizer:)))
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(changeOption1(tapGestureRecognizer:)))
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(changeOption2(tapGestureRecognizer:)))
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(changeOption3(tapGestureRecognizer:)))
        let callAction = UITapGestureRecognizer(target: self, action: #selector(callAction(tapGestureRecognizer:)))
        
        
        imageWeb.addGestureRecognizer(gesture0)
        imageWhats.addGestureRecognizer(gesture1)
        imageFacebook.addGestureRecognizer(gesture2)
        imageInstagram.addGestureRecognizer(gesture3)
        callIcon.addGestureRecognizer(callAction)
        
        
        let basicInformationGesture = UITapGestureRecognizer(target: self, action: #selector(openBasicInformation(tapGestureRecognizer:)))
        basicInformationIcon.addGestureRecognizer(basicInformationGesture)
        
        let ratingGesture = UITapGestureRecognizer(target: self, action: #selector(openRating(tapGestureRecognizer:)))
        ratingIcon.addGestureRecognizer(ratingGesture)
        
        let gestureToZoomImage = UITapGestureRecognizer(target: self, action: #selector(DetailDataImage.didTapToZoomImage))
        imagePrincipal.addGestureRecognizer(gestureToZoomImage)
    }
    
   
    
    @objc func callAction(tapGestureRecognizer: UITapGestureRecognizer) {
        if objectForLinks?.flwhatsappd3bc471ced == "" || objectForLinks?.flwhatsappd3bc471ced == nil {
            self.present(AlertManager.shared.defaultalert(title: titleAlertError, message: messageErrorNodataMedia), animated: true, completion: nil)
        }
        guard let adData = objectForLinks else { return }
        guard let phone = adData.flwhatsappd3bc471ced else { return }
        _ = ApiManager.shared.insertClic(adId: "\(objectForLinks?.flidentificador9a5668476f ?? 0)" , userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .call )
        print("Deberia abrir whatsapp")
        print(URL(string: "tel://52\(phone.suffix(10))"))
        if let url = URL(string: "tel://52\(phone.suffix(10))") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
           }
    }
    
    @objc func openBasicInformation(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let adData = objectForLinks else { return }
        let modal = ModalController()
        modal.loadBasicInformation(currentAd: adData)
        modal.modalPresentationStyle = .fullScreen
        self.navigationController?.present(modal, animated: true, completion: nil)
    }
    
    @objc func openRating(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let adData = objectForLinks else { return }
        let modal = ModalRatingController()
        modal.loadBasicInformation(currentAd: adData)
        modal.loadFrame(identificador: adData.flidentificador9a5668476f ?? 0)
        modal.modalPresentationStyle = .fullScreen
        self.navigationController?.present(modal, animated: true, completion: nil)
    }
    
    @objc func changeOption0(tapGestureRecognizer: UITapGestureRecognizer) {
        let currentUser = DataManager.shared.getDataUser()
        guard let url = URL(string: objectForLinks?.flpaginawebd22c4b4c53 ?? "") else { return }
        _ = ApiManager.shared.insertClic(adId: "\(objectForLinks?.flidentificador9a5668476f ?? 0)" , userId: "\(currentUser.flidentificadore7dbf413f9 ?? 0)", type: .web )
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }

    @objc func changeOption1(tapGestureRecognizer: UITapGestureRecognizer) {
        if objectForLinks?.flwhatsappgo9b768cb25f == "" || objectForLinks?.flwhatsappgo9b768cb25f == nil {
            self.present(AlertManager.shared.defaultalert(title: titleAlertError, message: messageErrorNodataMedia), animated: true, completion: nil)
        }
        _ = ApiManager.shared.insertClic(adId: "\(objectForLinks?.flidentificador9a5668476f ?? 0)" , userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .whatsapp )
        guard let whatsAppWithMessage = objectForLinks?.flwhatsapp5227d3664b73 else { return }
        guard let url = URL(string: whatsAppWithMessage.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? "") else { return }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    @objc func changeOption2(tapGestureRecognizer: UITapGestureRecognizer) {
        if objectForLinks?.flfacebooke7b82defaa == "" || objectForLinks?.flfacebooke7b82defaa == nil {
            self.present(AlertManager.shared.defaultalert(title: titleAlertError, message: messageErrorNodataMedia), animated: true, completion: nil)
        }
        guard let url = URL(string: objectForLinks?.flfacebooke7b82defaa ?? "") else { return }
        _ = ApiManager.shared.insertClic(adId: "\(objectForLinks?.flidentificador9a5668476f ?? 0)" , userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .facebook )
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    @objc func changeOption3(tapGestureRecognizer: UITapGestureRecognizer) {
        if objectForLinks?.flinstagram8b80438c8d == "" || objectForLinks?.flinstagram8b80438c8d == nil {
            self.present(AlertManager.shared.defaultalert(title: titleAlertError, message: messageErrorNodataMedia), animated: true, completion: nil)
        }
        guard let url = URL(string: objectForLinks?.flinstagram8b80438c8d ?? "") else { return }
        _ = ApiManager.shared.insertClic(adId: "\(objectForLinks?.flidentificador9a5668476f ?? 0)" , userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .instagram )
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            scroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0.0),
            scroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0.0),
            scroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0.0),
            scroll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0)
        ])
        
        NSLayoutConstraint.activate([
            titleLB.topAnchor.constraint(equalTo: self.scroll.topAnchor, constant: 8.0),
            titleLB.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.90),
            titleLB.centerXAnchor.constraint(equalTo: self.scroll.centerXAnchor, constant: 0),
        ])
        
        NSLayoutConstraint.activate([
            imagePrincipal.topAnchor.constraint(equalTo: self.titleLB.bottomAnchor, constant: 8.0),
            imagePrincipal.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.35),
            imagePrincipal.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.90),
            imagePrincipal.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            hereText.topAnchor.constraint(equalTo: self.imagePrincipal.bottomAnchor, constant: 0),
            hereText.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stackMedia0.topAnchor.constraint(equalTo: self.hereText.bottomAnchor, constant: 8.0),
            stackMedia0.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05),
            stackMedia0.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0)
        ])
        
        NSLayoutConstraint.activate([
            callIcon.topAnchor.constraint(equalTo: self.stackMedia0.bottomAnchor, constant: 8.0),
            callIcon.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.10),
            callIcon.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.30),
            callIcon.centerXAnchor.constraint(equalTo: self.scroll.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            basicInformationIcon.topAnchor.constraint(equalTo: self.callIcon.bottomAnchor, constant: 8.0),
            basicInformationIcon.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.13),
            basicInformationIcon.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.55),
            basicInformationIcon.centerXAnchor.constraint(equalTo: self.scroll.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            slideshow.topAnchor.constraint(equalTo: self.basicInformationIcon.bottomAnchor, constant: 8.0),
            slideshow.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.95),
            slideshow.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.30),
            slideshow.centerXAnchor.constraint(equalTo: self.scroll.centerXAnchor),
        ])
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.slideshow.bottomAnchor, constant: 8.0),
            contentView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.10),
            contentView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.95),
            contentView.centerXAnchor.constraint(equalTo: self.scroll.centerXAnchor),
        ])
        
        NSLayoutConstraint.activate([
            favoriteLB.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 1.0),
            favoriteLB.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 4.0),
            favoriteLB.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -1.0),
            favoriteLB.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.70)
        ])
        
        NSLayoutConstraint.activate([
            imageFavorite.widthAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.50),
            imageFavorite.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.50),
            imageFavorite.centerYAnchor.constraint(equalTo: self.favoriteLB.centerYAnchor, constant: 0.0),
            imageFavorite.leadingAnchor.constraint(equalTo: self.favoriteLB.trailingAnchor, constant: 3.0)
        ])
        
        NSLayoutConstraint.activate([
            countComentaries.bottomAnchor.constraint(equalTo: self.ratintView.topAnchor, constant: 0),
            countComentaries.leadingAnchor.constraint(equalTo: self.ratintView.leadingAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            ratintView.topAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 8.0),
            ratintView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.50),
            ratintView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.10),
            ratintView.leadingAnchor.constraint(equalTo: self.favoriteLB.leadingAnchor,constant: 50),
        ])
        NSLayoutConstraint.activate([
            ratingIcon.topAnchor.constraint(equalTo: self.ratintView.bottomAnchor, constant: 8.0),
            ratingIcon.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.13),
            ratingIcon.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.55),
            ratingIcon.centerXAnchor.constraint(equalTo: self.scroll.centerXAnchor),
        ])
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: self.ratingIcon.bottomAnchor, constant: 8.0),
            descriptionLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8.00),
            descriptionLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8.00)
        ])
        NSLayoutConstraint.activate([
            frameVideo.topAnchor.constraint(equalTo: self.descriptionLabel.bottomAnchor, constant: 12),
            frameVideo.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.40),
            frameVideo.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.95),
            frameVideo.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            frameVideo.bottomAnchor.constraint(equalTo: self.scroll.bottomAnchor,constant: UIScreen.main.bounds.height * 0.00),
        ])
        NSLayoutConstraint.activate([
            indicatorBottomCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            indicatorBottomCollection.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0),
            indicatorBottomCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.95),
            indicatorBottomCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15)
        ])
    }
    
    func otherOptionSlideShow() {
        slideshow.delegate = self
        slideshow.setImageInputs(localSource)
    }
    
}

extension DetailDataImage: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrayBottomTabImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  ImageCategoryCell.id, for: indexPath) as! ImageCategoryCell
            cell.backgroundColor = .clear
            cell.imageCatalog.backgroundColor = .clear
            cell.imageCatalog.image = UIImage(named: arrayBottomTabImages[indexPath.row])
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCategoryCell {
                cell.backgroundColor = .clear
                cell.imageCatalog.backgroundColor = .clear
                cell.imageCatalog.image = UIImage(named: arraySelectedTabImages[indexPath.row])
                toggleOption(option: indexPath.row)
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCategoryCell {
                cell.backgroundColor = .clear
                cell.imageCatalog.backgroundColor = .clear
                cell.imageCatalog.image = UIImage(named: arrayBottomTabImages[indexPath.row])
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wCell = view.frame.height * 0.10
        let hCell = view.frame.height * 0.10
        return CGSize(width: wCell, height: hCell)
    }
    
    func toggleOption(option: Int) {
        switch option {
        case 0:
            guard let curentControllers = self.navigationController?.viewControllers else { return }
            let home = curentControllers[1]
            self.navigationController?.popToViewController(home, animated: true)
        case 1:
            print("[option] [value] [\(option)]")
        case 2:
            print("[option] [value] [\(option)]")
        case 3:
            print("[option] [value] [\(option)]")
        case 4:
            print("[option] [value] [\(option)]")
        case 5:
            print("[option] [value] [\(option)]")
        default:
            print("do something")
        }
    }
    
}

extension DetailDataImage: ImageSlideshowDelegate {
    
}

extension DetailDataImage: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let currentUser = DataManager.shared.getDataUser()
        if searchBar.text != "" &&  searchBar.text != " " {
            self.showloader()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let busqueda = ApiManager.shared.searchqueryWs(zonaId: currentUser.flzona621bfe9801 ?? 0, word: searchBar.text ?? "")
                self.hideloader()
                let searchScreen = SearchVC()
                searchScreen.modalPresentationStyle = .fullScreen
                searchScreen.searchArray = busqueda
                self.tabBarController?.navigationController?.pushViewController(searchScreen, animated: true)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        if searchBar.text != "" &&  searchBar.text != " " {
//            self.showloader()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                let busqueda = ApiManager.shared.searchqueryWs(word: searchBar.text ?? "")
//                self.hideloader()
//                let searchScreen = SearchVC()
//                searchScreen.modalPresentationStyle = .fullScreen
//                searchScreen.searchArray = busqueda
//                self.tabBarController?.navigationController?.pushViewController(searchScreen, animated: true)
//            }
//        }
    }
    
}

extension DetailDataImage: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to url \(String(describing: webView.url))")
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let response = navigationResponse.response as? HTTPURLResponse {
                print(" Status Code ",response.statusCode)
            }
            decisionHandler(.allow)
    }

}
