import Foundation
import UIKit
import SideMenuSwift
import SideMenu
import Kingfisher

protocol RefreshDataUser {
    func reloadDataCurrentUser()
}


class HomeCategory: UIViewController {
    
    var hidebar: Bool = false
    let searchController = UISearchController(searchResultsController: nil)
    var nameImages: [String] = ["img10904","img10903","img10902"]
    var currentCategories = ApiManager.shared.categoriesWs()
    var allAnunciosArray: [WelcomeResponse] = ApiManager.shared.welcomeWs()
    
    lazy var messageRegister: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Favor de crear su perfil para ofrecerle anuncios personalizados a su zona"
        properties.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        properties.textColor = primaryColorGreen
        properties.numberOfLines = 0
        properties.textAlignment = .justified
        return properties
    }()
    
    lazy var registerButton: UIButton = {
        let properties = UIButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setTitle("Registrarse", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.setTitleColor(.white, for: .normal)
        properties.titleLabel?.textColor = .white
        properties.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        return properties
    }()
    
    lazy var bottomMessageRegisterView: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.85)
        return properties
    }()
    
    lazy var scroll: UIScrollView = {
        let config = UIScrollView()
        config.translatesAutoresizingMaskIntoConstraints = false
        config.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        return config
    }()
    
    lazy var contentView: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .clear
        return properties
    }()
    
    var imageTop: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "img10917")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var saludoText: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = " Hola ¿ en que podemos servirte hoy ? "
        properties.numberOfLines = 0
        properties.font = UIFont(name: "Helvetica-bold", size: 15)
        properties.backgroundColor = .clear
        properties.textAlignment = .center
        properties.textColor = .black
        return properties
    }()
    
    let menuCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 15
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(ImageCategoryCell.self, forCellWithReuseIdentifier: ImageCategoryCell.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        listOptions.backgroundColor = .clear
        return listOptions
    }()
    
    lazy var contentBottomView: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .clear
        return properties
    }()
    
    var welcomeTitleBottomText: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = ""
        properties.numberOfLines = 0
        properties.font = UIFont(name: "Helvetica-bold", size: 17)
        properties.backgroundColor = .clear
        properties.textAlignment = .center
        properties.textColor = primaryColorGreen
        return properties
    }()
    
    var welcomeBottomText: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = ""
        properties.numberOfLines = 0
        properties.font = UIFont(name: "Helvetica-bold", size: 17)
        properties.backgroundColor = .clear
        properties.textAlignment = .center
        properties.textColor = .black
        return properties
    }()
    
    var imageBottom: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "placeholderImage")
        properties.contentMode = .scaleAspectFit
        properties.backgroundColor = .clear
        return properties
    }()
    
    let todosLosAnunciosCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(HomeCellMessage.self, forCellWithReuseIdentifier: HomeCellMessage.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = false
        listOptions.tag = 0
        listOptions.backgroundColor = UIColor.clear
        listOptions.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        listOptions.backgroundColor = .clear
        return listOptions
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addScroll()
        autolayout()
        delegatesCollection()
        setNavItems()
        
        if DataManager.shared.isInvited().0 {
            setModalUserInvited()
            showScreenZone()
        } else {
            setTextSaludo()
        }
    }
    
    // MARK: in case invited show the screen to choose zone
    
    func showScreenZone() {
        if DataManager.shared.isInvited().0 && DataManager.shared.isInvited().1 == true  {
            let registerController = PerfilViewController()
            registerController.flag = .myProfile
            self.navigationController?.pushViewController(registerController, animated: true)
        }
    }
    
    func setModalUserInvited() {
        addTargetRegisterButton()
        self.view.addSubview(bottomMessageRegisterView)
        self.bottomMessageRegisterView.addSubview(messageRegister)
        self.bottomMessageRegisterView.addSubview(registerButton)
        
        NSLayoutConstraint.activate([
            bottomMessageRegisterView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.20),
            bottomMessageRegisterView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
            bottomMessageRegisterView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            bottomMessageRegisterView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            messageRegister.topAnchor.constraint(equalTo: self.bottomMessageRegisterView.topAnchor, constant: 8.0),
            messageRegister.leadingAnchor.constraint(equalTo: self.bottomMessageRegisterView.leadingAnchor, constant: 8),
            messageRegister.trailingAnchor.constraint(equalTo: self.bottomMessageRegisterView.trailingAnchor, constant: -8),
            messageRegister.bottomAnchor.constraint(equalTo: self.registerButton.topAnchor, constant: 8),
        ])
        
        NSLayoutConstraint.activate([
            registerButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.70),
            registerButton.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05),
            registerButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            registerButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -14.0)
        ])
    }
    
    func removeModalUserInvited() {
        bottomMessageRegisterView.removeFromSuperview()
        messageRegister.removeFromSuperview()
        registerButton.removeFromSuperview()
    }
    
    func addTargetRegisterButton() {
        registerButton.addTarget(self, action: #selector(registerToogle), for: .touchUpInside)
    }
    
    @objc func registerToogle() {
        let nexvc = PerfilViewController()
        nexvc.delegateRefreshProfile = self
        nexvc.flag = .register
        self.navigationController?.pushViewController(nexvc, animated: true)
    }
    
    private func setTextSaludo() {
        let currentUser = DataManager.shared.getDataUser()
        let textSaludo = ApiManager.shared.saludoInicial(identificador: currentUser.flidentificadore7dbf413f9 ?? 0)
        guard let text = textSaludo.first else { return }
        saludoText.text = text.flsaludoinicio801ebafd9e
    }
    
    private func setNavItems() {
        let menuIcon = UIImage(named: "134216-24")
        let searchIcon = UIImage(named: "3844432-24")
        let menuItem = UIBarButtonItem(image: menuIcon?.withRenderingMode(.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
        let searchItem = UIBarButtonItem(image: searchIcon?.withRenderingMode(.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(searchAction))
        searchItem.tintColor = UIColor.white
        menuItem.tintColor = UIColor.white
        if let navBar = self.navigationController?.navigationBar {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = primaryColorGreen
            navBar.standardAppearance = appearance
            navBar.scrollEdgeAppearance = navBar.standardAppearance
        }
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        navigationItem.leftBarButtonItem = .some(UIBarButtonItem())
        navigationItem.leftBarButtonItem = menuItem
        navigationItem.rightBarButtonItem = searchItem
        navigationItem.searchController = nil
        searchController.searchBar.delegate = self
    }
    
    func lateralMenu() {
        let controllerMenu = MenuController()
        controllerMenu.delegateOptionMenu = self
        let menu = SideMenuNavigationController(rootViewController: controllerMenu)
        menu.leftSide = true
        menu.statusBarEndAlpha = 0
        self.present(menu, animated: true, completion: nil)
    }
    
    @objc func menuPressed() {
        lateralMenu()
    }
    
    @objc func searchAction() {
        if !hidebar {
            navigationItem.searchController = searchController
        } else {
            navigationItem.searchController = nil
        }
        hidebar = !hidebar
    }
    
    private func addScroll() {
        self.scroll.delegate = self
        self.view.addSubview(scroll)
        self.scroll.addSubview(contentView)
        self.contentView.addSubview(imageTop)
        self.contentView.addSubview(saludoText)
        self.scroll.addSubview(menuCollection)
        self.scroll.addSubview(todosLosAnunciosCollection)
    }
    
    private func autolayout() {
        
        print(UIDevice.current.screenType.rawValue)
        
        if UIDevice.current.screenType.rawValue.contains("XR") ||  UIDevice.current.screenType.rawValue.contains("12") || UIDevice.current.screenType.rawValue.contains("X") {
            
            NSLayoutConstraint.activate([
                scroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0.0),
                scroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
                scroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
                scroll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
            ])
            NSLayoutConstraint.activate([
                contentView.topAnchor.constraint(equalTo: self.scroll.topAnchor, constant: 0),
                contentView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.18),
                contentView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.90),
                contentView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            ])
            NSLayoutConstraint.activate([
                imageTop.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 1),
                imageTop.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 8.0),
                imageTop.trailingAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: -8.0),
                imageTop.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
            ])
            NSLayoutConstraint.activate([
                saludoText.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 1.0),
                saludoText.leadingAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 8.0),
                saludoText.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8.0),
                saludoText.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
            ])
            
            NSLayoutConstraint.activate([
                menuCollection.topAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 12.0),
                menuCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.45),
                menuCollection.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0),
                menuCollection.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0),
            ])
            
            NSLayoutConstraint.activate([
                todosLosAnunciosCollection.topAnchor.constraint(equalTo: self.menuCollection.bottomAnchor,constant: -8),
                todosLosAnunciosCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
                todosLosAnunciosCollection.heightAnchor.constraint(equalToConstant: CGFloat(allAnunciosArray.count) * 250.00),
                todosLosAnunciosCollection.bottomAnchor.constraint(equalTo: self.scroll.bottomAnchor, constant: 0.0)
            ])
            
        }
        
        if UIDevice.current.screenType.rawValue.contains("iPhone 6") || UIDevice.current.screenType.rawValue.contains("iPhone 6S") || UIDevice.current.screenType.rawValue.contains("iPhone 7") || UIDevice.current.screenType.rawValue.contains("iPhone 8") {
            NSLayoutConstraint.activate([
                scroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0.0),
                scroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
                scroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
                scroll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
            ])
            NSLayoutConstraint.activate([
                contentView.topAnchor.constraint(equalTo: self.scroll.topAnchor, constant: 0),
                contentView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.18),
                contentView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.90),
                contentView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            ])
            NSLayoutConstraint.activate([
                imageTop.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 1),
                imageTop.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 8.0),
                imageTop.trailingAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: -8.0),
                imageTop.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
            ])
            NSLayoutConstraint.activate([
                saludoText.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 1.0),
                saludoText.leadingAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 8.0),
                saludoText.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8.0),
                saludoText.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
            ])
            NSLayoutConstraint.activate([
                menuCollection.topAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 12.0),
                menuCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.50),
                menuCollection.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0),
                menuCollection.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0),
            ])
            
            NSLayoutConstraint.activate([
                todosLosAnunciosCollection.topAnchor.constraint(equalTo: self.menuCollection.bottomAnchor,constant: -8),
                todosLosAnunciosCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
                todosLosAnunciosCollection.heightAnchor.constraint(equalToConstant: CGFloat(allAnunciosArray.count) * 250.00),
                todosLosAnunciosCollection.bottomAnchor.constraint(equalTo: self.scroll.bottomAnchor, constant: 0.0)
            ])
        }
            
    }
    
}

extension HomeCategory: RefreshDataUser {
    
    func reloadDataCurrentUser() {
        print("[reloadDataCurrentUser]")
        if !DataManager.shared.isInvited().0 {
            removeModalUserInvited()
        }
        setTextSaludo()
    }

}


extension HomeCategory: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func delegatesCollection() {
        menuCollection.delegate = self
        menuCollection.dataSource = self
        todosLosAnunciosCollection.delegate = self
        todosLosAnunciosCollection.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == todosLosAnunciosCollection {
            return allAnunciosArray.count
        }
        return nameImages.count //currentCategories.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == todosLosAnunciosCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  HomeCellMessage.id, for: indexPath) as! HomeCellMessage
            cell.backgroundColor = .clear
            cell.titleAnuncio.text = allAnunciosArray[indexPath.row].fltitulod6250a7b31 ?? "_"
            cell.descriptionAnuncio.text = allAnunciosArray[indexPath.row].fldescripcion639c11e75c ?? "_"
            print(allAnunciosArray[indexPath.row].fldescripcion639c11e75c ?? "_")
            let path = allAnunciosArray[indexPath.row].flimagenview3a83e2f814 ?? ""
            let generalpath = baseUrl + path
            cell.imageAnuncio.kf.setImage(with: URL(string: generalpath))
            if UIDevice.current.screenType.rawValue.contains("XR") {
                cell.descriptionAnuncio.font = UIFont.systemFont(ofSize: 14)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  ImageCategoryCell.id, for: indexPath) as! ImageCategoryCell
            cell.configure(for: nameImages[indexPath.row])
            return cell
        }
    }
     
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != todosLosAnunciosCollection {
            let optionsTitle: [Int] = [1,2,3]
            let detailVc = TabBarContainerController()
            detailVc.idCategorySelected = optionsTitle[indexPath.row]
            self.navigationController?.pushViewController(detailVc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // MARK: Este collection es el que aparece hasta abajo de las 3 categorías
        if collectionView == todosLosAnunciosCollection {
            if UIDevice.current.screenType.rawValue.contains("XR") {
                let wCell = view.frame.width * 0.70
                let hCell = view.frame.height * 0.30
                return CGSize(width: wCell, height: hCell)
            }
            let wCell = view.frame.width * 0.95
            return CGSize(width: wCell, height: view.frame.height * 0.35)
        }
        // MARK: Tamaño de las categorías como comida | servicios | productos
        if UIDevice.current.screenType.rawValue.contains("XR") || UIDevice.current.screenType.rawValue.contains("12") || UIDevice.current.screenType.rawValue.contains("X") || UIDevice.current.screenType.rawValue.contains("Pro") || UIDevice.current.screenType.rawValue.contains("11") ||
            UIDevice.current.screenType.rawValue.contains("Plus")
            {
            let wCell = view.frame.width * 0.85
            let hCell = view.frame.height * 0.12
            return CGSize(width: wCell, height: hCell)
        }
        let wCell = view.frame.width * 0.70
        let hCell = view.frame.height * 0.13
        return CGSize(width: wCell, height: hCell)
        
    }
    
}

extension HomeCategory: optionMenu {
    func changeScreen(option: Int) {
        switch option {
        case 0:
            let nexvc = PerfilViewController()
            nexvc.delegateRefreshProfile = self
            nexvc.flag = .myProfile
            self.navigationController?.pushViewController(nexvc, animated: true)
        case 1:
            self.navigationController?.pushViewController(HomeCategory(), animated: true)
        case 2:
            self.navigationController?.pushViewController(FavoritesController(), animated: true)
        case 3:
            self.navigationController?.pushViewController(RegistrationWebController(), animated: true)
        case 4:
            if DataManager.shared.isInvited().0 {
                let registerController = PerfilViewController()
                registerController.delegateRefreshProfile = self
                registerController.flag = .register
                self.navigationController?.pushViewController(registerController, animated: true)
            } else {
                DataManager.shared.closeLogin(ifCloseSession: true)
                DataManager.shared.removeUser()
                self.navigationController?.popToRootViewController(animated: true)
                self.navigationController?.setViewControllers([LoginController()], animated: true)
            }
        default:
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension HomeCategory: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        let currentUser = DataManager.shared.getDataUser()
        if searchBar.text != "" &&  searchBar.text != " " {
            self.showloader()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let busqueda = ApiManager.shared.searchqueryWs(zonaId: currentUser.flzona621bfe9801 ?? 0, word: searchBar.text ?? "")
                self.hideloader()
                let searchScreen = SearchVC()
                searchScreen.modalPresentationStyle = .fullScreen
                searchScreen.searchArray = busqueda
                self.navigationController?.pushViewController(searchScreen, animated: true)
            }
        }
    }
    
}

extension UIViewController {
    
    public static let child = SpinnerViewController()

    public func showloader() {
        addChild(UIViewController.child)
        UIViewController.child.view.frame = view.frame
        view.addSubview(UIViewController.child.view)
        UIViewController.child.didMove(toParent: self)
    }
    
    public func hideloader() {
        UIViewController.child.willMove(toParent: nil)
        UIViewController.child.view.removeFromSuperview()
        UIViewController.child.removeFromParent()
    }
    
}

extension HomeCategory: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // This method is called as the user scrolls
        
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 2.0) {
            self.bottomMessageRegisterView.alpha = 0.0
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // This method is called right as the user lifts their finger
        UIView.animate(withDuration: 3.0) {
            self.bottomMessageRegisterView.alpha = 1.0
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // This method is called when the scrollview finally stops scrolling.
        UIView.animate(withDuration: 3.0) {
            self.bottomMessageRegisterView.alpha = 1.0
        }
    }
    
}
