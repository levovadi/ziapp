
import UIKit
import Foundation
class HomeCellMessage: UICollectionViewCell {
    
    static let id = "idCellImageAndTitle"
    
    var titleAnuncio: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = ""
        properties.textColor = primaryColorGreen
        properties.sizeToFit()
        properties.font = UIFont.boldSystemFont(ofSize: 23)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var descriptionAnuncio: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = ""
        properties.textColor = .black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 12)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()

    let imageAnuncio: UIImageView = {
        let properties = UIImageView()
        properties.image = #imageLiteral(resourceName: "img10917")
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var bottomLine: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .lightGray
        return properties
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        addSubviews()
        setAutolayout()
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        self.addSubview(titleAnuncio)
        self.addSubview(descriptionAnuncio)
        self.addSubview(imageAnuncio)
        self.addSubview(bottomLine)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            titleAnuncio.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleAnuncio.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            titleAnuncio.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        NSLayoutConstraint.activate([
            descriptionAnuncio.topAnchor.constraint(equalTo: self.titleAnuncio.bottomAnchor, constant: 8),
            descriptionAnuncio.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            descriptionAnuncio.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
        ])
        NSLayoutConstraint.activate([
            imageAnuncio.topAnchor.constraint(equalTo: self.descriptionAnuncio.bottomAnchor, constant: 1),
            imageAnuncio.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.50),
            imageAnuncio.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.75),
            imageAnuncio.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageAnuncio.bottomAnchor.constraint(equalTo: self.bottomLine.topAnchor, constant: -8)
        ])
        NSLayoutConstraint.activate([
            bottomLine.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.010),
            bottomLine.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.20),
            bottomLine.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0),
            bottomLine.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0)
        ])
    }
    
}
