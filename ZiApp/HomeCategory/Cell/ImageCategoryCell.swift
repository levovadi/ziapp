import Foundation
import UIKit
import Kingfisher

class ImageCategoryCell: UICollectionViewCell {
    
    static let id = "idCategoryCell"
    
    let imageCatalog: UIImageView = {
        let properties = UIImageView()
        properties.image = #imageLiteral(resourceName: "placeholderImage")
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    func configure(for nameImage: String) {
        imageCatalog.image = UIImage(named: nameImage)
        imageCatalog.contentMode = .scaleToFill
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageCatalog)
        self.backgroundColor = .white
        setAutolayout()
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            imageCatalog.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            imageCatalog.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            imageCatalog.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            imageCatalog.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0),
        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
