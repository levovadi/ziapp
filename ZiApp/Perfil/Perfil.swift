
import Foundation
import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

enum typeProfileScreen {
    case firstInstall
    case register
    case myProfile
}

class PerfilViewController: UIViewController {
    
    var delegateRefreshProfile: RefreshDataUser?
    var delegate: makeAutoLogin?
    var showData: [ZonaResponse] = []
    var zoneId: Int = 0
    var flag: typeProfileScreen = .register
    let apiCalls = ApiManager()
    
    var titleLB: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Mi perfil"
        properties.textColor = .green
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 17)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var nameInput: MDCFilledTextField = {
        let properties = MDCFilledTextField()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.tag = 0
        properties.textColor = .blue
        properties.label.text = "Nombre"
        properties.setTextColor(.red, for: .normal)
        properties.setTextColor(.blue, for: .disabled)
        properties.setTextColor(.yellow, for: .editing)
        properties.label.highlightedTextColor = UIColor.red
        properties.label.textColor = .black
        properties.label.textAlignment = .left
        properties.placeholder = "Jackson Five"
        properties.leadingAssistiveLabel.translatesAutoresizingMaskIntoConstraints = false
        properties.leadingAssistiveLabel.highlightedTextColor = .blue
        properties.leadingAssistiveLabel.textAlignment = .center
        properties.textAlignment = .left
        properties.sizeToFit()
        properties.setUnderlineColor(.black, for: .normal)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setUnderlineColor(.black, for: .disabled)
        properties.setFilledBackgroundColor(.clear, for: .normal)
        properties.setFilledBackgroundColor(.clear, for: .editing)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setTextColor(.black, for: .editing)
        properties.setTextColor(.black, for: .disabled)
        properties.setTextColor(.black, for: .normal)
        properties.textAlignment = .left
        properties.tag = 0
        properties.label.textColor = .black
        properties.setFloatingLabelColor(.black, for: .disabled)
        properties.setFloatingLabelColor(.black, for: .editing)
        properties.setFloatingLabelColor(.black, for: .normal)
        properties.setNormalLabelColor(.black, for: .normal)
        return properties
    }()
    
    var mailInput: MDCFilledTextField = {
        let properties = MDCFilledTextField()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.tag = 0
        properties.textColor = .blue
        properties.label.text = "Correo electrónico"
        properties.setTextColor(.red, for: .normal)
        properties.setTextColor(.blue, for: .disabled)
        properties.setTextColor(.yellow, for: .editing)
        properties.label.highlightedTextColor = UIColor.red
        properties.label.textColor = .black
        properties.label.textAlignment = .left
        properties.placeholder = "example@domain.com"
        properties.leadingAssistiveLabel.translatesAutoresizingMaskIntoConstraints = false
        properties.leadingAssistiveLabel.highlightedTextColor = .blue
        properties.leadingAssistiveLabel.textAlignment = .center
        properties.textAlignment = .left
        properties.sizeToFit()
        properties.setUnderlineColor(.black, for: .normal)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setUnderlineColor(.black, for: .disabled)
        properties.setFilledBackgroundColor(.clear, for: .normal)
        properties.setFilledBackgroundColor(.clear, for: .editing)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setTextColor(.black, for: .editing)
        properties.setTextColor(.black, for: .disabled)
        properties.setTextColor(.black, for: .normal)
        properties.textAlignment = .left
        properties.tag = 0
        properties.label.textColor = .black
        properties.setFloatingLabelColor(.black, for: .disabled)
        properties.setFloatingLabelColor(.black, for: .editing)
        properties.setFloatingLabelColor(.black, for: .normal)
        properties.setNormalLabelColor(.black, for: .normal)
        return properties
    }()
    
    var pwdInput: MDCFilledTextField = {
        let properties = MDCFilledTextField()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.isSecureTextEntry = true
        properties.tag = 0
        properties.textColor = .blue
        properties.label.text = "Contraseña"
        properties.setTextColor(.red, for: .normal)
        properties.setTextColor(.blue, for: .disabled)
        properties.setTextColor(.yellow, for: .editing)
        properties.label.tintColor = .red
        properties.label.highlightedTextColor = .blue
        properties.label.highlightedTextColor = UIColor.red
        properties.label.textAlignment = .center
        properties.placeholder = "ABCdef123."
        properties.leadingAssistiveLabel.translatesAutoresizingMaskIntoConstraints = false
        properties.leadingAssistiveLabel.highlightedTextColor = .blue
        properties.leadingAssistiveLabel.textAlignment = .center
        properties.textAlignment = .left
        properties.sizeToFit()
        properties.setUnderlineColor(.black, for: .normal)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setUnderlineColor(.black, for: .disabled)
        properties.setFilledBackgroundColor(.clear, for: .normal)
        properties.setFilledBackgroundColor(.clear, for: .editing)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setTextColor(.black, for: .editing)
        properties.setTextColor(.black, for: .disabled)
        properties.setTextColor(.black, for: .normal)
        properties.textAlignment = .left
        properties.tag = 0
        properties.label.textColor = .black
        properties.setFloatingLabelColor(.black, for: .disabled)
        properties.setFloatingLabelColor(.black, for: .editing)
        properties.setFloatingLabelColor(.black, for: .normal)
        properties.setNormalLabelColor(.black, for: .normal)
        return properties
    }()
    
    var zonaInput: MDCFilledTextField = {
        let properties = MDCFilledTextField()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.isSecureTextEntry = false
        properties.tag = 0
        properties.textColor = .blue
        properties.label.text = "Zonas"
        properties.setTextColor(.red, for: .normal)
        properties.setTextColor(.blue, for: .disabled)
        properties.setTextColor(.yellow, for: .editing)
        properties.label.tintColor = .red
        properties.label.highlightedTextColor = .blue
        properties.label.highlightedTextColor = UIColor.red
        properties.label.textAlignment = .center
        properties.placeholder = "elige tu zona"
        properties.leadingAssistiveLabel.translatesAutoresizingMaskIntoConstraints = false
        properties.leadingAssistiveLabel.highlightedTextColor = .blue
        properties.leadingAssistiveLabel.textAlignment = .center
        properties.textAlignment = .center
        properties.sizeToFit()
        properties.setUnderlineColor(.black, for: .normal)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setUnderlineColor(.black, for: .disabled)
        properties.setFilledBackgroundColor(.clear, for: .normal)
        properties.setFilledBackgroundColor(.clear, for: .editing)
        properties.setUnderlineColor(.black, for: .editing)
        properties.setTextColor(.black, for: .editing)
        properties.setTextColor(.black, for: .disabled)
        properties.setTextColor(.black, for: .normal)
        properties.textAlignment = .center
        properties.tag = 0
        properties.label.textColor = .black
        properties.setFloatingLabelColor(.black, for: .disabled)
        properties.setFloatingLabelColor(.black, for: .editing)
        properties.setFloatingLabelColor(.black, for: .normal)
        properties.setNormalLabelColor(.black, for: .normal)
        return properties
    }()
    
    let saveButton: MDCButton = {
        let properties = MDCButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        properties.setTitle("guardar", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.inkColor = UIColor.green
        properties.tintColor = UIColor.systemGreen
        properties.rippleColor = primaryColorGreen
        return properties
    }()
    
    var pickerSelection: UIPickerView = {
        let properties = UIPickerView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        return properties
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        addSubviews()
        setDataScreenProfile()
        if flag == .register {
            setAutolayout()
        } else if flag == .myProfile {
            if DataManager.shared.isInvited().0 {
                hideTextfields()
                setAutolayoutInvited()
                DataManager.shared.setFlagInvited(value: true, isFirstTimeInvited: false)
            } else {
                setAutolayout()
            }
        } else {
            if DataManager.shared.isInvited().0 {
                hideTextfields()
                setAutolayoutInvited()
            }
        }
    }
    
    func hideTextfields() {
        self.nameInput.alpha = 0
        self.pwdInput.alpha = 0
        self.mailInput.alpha = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Perfil"
        dismissKeyboardsGesture()
        initPickerView()
        addTarget()
        showData = ApiManager.shared.getZonasWs()
        pickerSelection.reloadInputViews()
        pickerSelection.reloadAllComponents()
        setDataScreenProfile()
    }
    
    func addTarget() {
        saveButton.addTarget(self, action: #selector(registerAction), for: .touchUpInside)
    }
    
    func validateEmptyFields() -> Bool {
        if nameInput.text == ""  || mailInput.text == "" || pwdInput.text == "" || zonaInput.text == "" {
            let alert = UIAlertController(title: "", message: "Todos los campos deben ser llenados", preferredStyle: .alert)
            let actionAccept = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            alert.addAction(actionAccept)
            self.present(alert, animated: true, completion: nil)
            return false
        } else {
            return true
        }
    }
    
    @objc func registerAction() {
        let currentUser = DataManager.shared.getDataUser()
        switch flag {
        case .firstInstall:
            let flagindicator = validateEmptyFields()
            if flagindicator {
                let result = ApiManager.shared.registerWs(name: nameInput.text ?? "", mail: mailInput.text ?? "", pwd:  pwdInput.text ?? "", zone: zoneId)
                if result.count > 0 {
                    DataManager.shared.setFlagInvited(value: false, isFirstTimeInvited: false)
                    let info = UIAlertController(title: "Registro correcto", message: "¿Deseas entrar a la aplicación?", preferredStyle: .alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
                        guard let mailLogin = result.first?.flcorreoelectronico57997cc6aa else { return }
                        guard let passLogin = result.first?.flcontrasenaa8f7f67349 else { return }
                        let Loginresult = self.apiCalls.loginWs(usermail: mailLogin, password: passLogin,isLogged: true)
                        if Loginresult.count > 0 {
                            DataManager.shared.firstEntry(isFirstEntry: false)
                            self.navigationController?.pushViewController(HomeCategory(), animated: true)
                        } else {
                            self.present(AlertManager.shared.defaultalert(title: "No encontrado", message: "Usuario no encontrado"), animated: true, completion: nil)
                        }
                    }
                    let cancelar = UIAlertAction(title: "Cancelar", style: .default) { (action) in
                        self.nameInput.text = ""
                        self.mailInput.text = ""
                        self.pwdInput.text = ""
                        self.zonaInput.text = ""
                        DataManager.shared.closeLogin(ifCloseSession: true)
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    info.addAction(aceptar)
                    info.addAction(cancelar)
                    self.present(info, animated: true, completion: nil)
            }
            } else {
                self.present(AlertManager.shared.defaultalert(title: "Info", message: "Ocurrio un error al intentar registrarse"), animated: true, completion: nil)
            }
        case .register:
            let flagindicator = validateEmptyFields()
            if flagindicator {
                let result = ApiManager.shared.registerWs(name: nameInput.text ?? "", mail: mailInput.text ?? "", pwd:  pwdInput.text ?? "", zone: zoneId)
                if result.count > 0 {
                    let userToSave = LoginResponse(flcontrasenaa8f7f67349: result[0].flcontrasenaa8f7f67349, flcorreoelectronico57997cc6aa: result[0].flcorreoelectronico57997cc6aa, flidentificadore7dbf413f9: result[0].flidentificadore7dbf413f9, flzona621bfe9801: result[0].flzona621bfe9801)
                    DataManager.shared.saveUserData(userData: userToSave)
                    DataManager.shared.setFlagInvited(value: false, isFirstTimeInvited: false)
                    let info = UIAlertController(title: "Registro correcto", message: "¿Deseas entrar a la aplicación?", preferredStyle: .alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
                        self.dismiss(animated: true, completion: nil)
                        guard let newuser = result.first else { return }
                        self.delegate?.loginWithUser(usermail: newuser.flcorreoelectronico57997cc6aa ?? "", pwd: newuser.flcontrasenaa8f7f67349 ?? "")
                        self.delegateRefreshProfile?.reloadDataCurrentUser()
                        self.navigationController?.popViewController(animated: true)                    }
                    let cancelar = UIAlertAction(title: "Cancelar", style: .default) { (action) in
                        self.dismiss(animated: true, completion: nil)
                        self.flag = .myProfile
                        self.setDataScreenProfile()
                    }
                    info.addAction(aceptar)
                    info.addAction(cancelar)
                    self.present(info, animated: true, completion: nil)
            }
            } else {
                self.present(AlertManager.shared.defaultalert(title: "Info", message: "Ocurrio un error al intentar registrarse"), animated: true, completion: nil)
            }
        case .myProfile:
            let result = ApiManager.shared.updateProfileData(name: nameInput.text ?? "", mail: mailInput.text ?? "", pwd:  pwdInput.text ?? "", zone: zoneId, sessionId: currentUser.flidentificadore7dbf413f9 ?? 0)
            if result.count > 0 {
                guard let newDataUser = result.first else { return }
                nameInput.text = newDataUser.flnombre504267dd91 ?? ""
                mailInput.text = newDataUser.flcorreoelectronico57997cc6aa ?? ""
                for item in showData {
                    if item.flidentificador6d8056256d == newDataUser.flzona621bfe9801 {
                        zonaInput.text = item.flzona0119225338
                    }
                }
                let saveUser = LoginResponse(flcontrasenaa8f7f67349: newDataUser.flcontrasenaa8f7f67349 ?? "", flcorreoelectronico57997cc6aa: newDataUser.flcorreoelectronico57997cc6aa ?? "", flidentificadore7dbf413f9: newDataUser.flidentificadore7dbf413f9 ?? 0, flzona621bfe9801: newDataUser.flzona621bfe9801 ?? 0)
                DataManager.shared.saveUserData(userData: saveUser)
                delegateRefreshProfile?.reloadDataCurrentUser()
                let alerta = UIAlertController(title: "", message: "Datos actualizados con éxito ", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (act) in
                    if DataManager.shared.isInvited().0 {
                        DataManager.shared.setFlagInvited(value: true, isFirstTimeInvited: false)
                        self.navigationController?.pushViewController(HomeCategory(), animated: true)
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                alerta.addAction(aceptar)
                self.present(alerta, animated: true, completion: nil)
            } else {
                self.present(AlertManager.shared.defaultalert(title: "Info", message: "Ocurrio un error al intentar registrarse"), animated: true, completion: nil)
            }
        }
        
    }
    
    func dismissKeyboardsGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func initPickerView() {
        pickerSelection.dataSource = self
        pickerSelection.delegate = self
        zonaInput.inputView = pickerSelection
//        pickerSelection.isHidden = true
        zonaInput.delegate = self
    }
    
    func addSubviews() {
        self.view.addSubview(titleLB)
        self.view.addSubview(nameInput)
        self.view.addSubview(mailInput)
        self.view.addSubview(pwdInput)
        self.view.addSubview(zonaInput)
        //self.view.addSubview(pickerSelection)
        self.view.addSubview(saveButton)
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            titleLB.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            titleLB.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            nameInput.topAnchor.constraint(equalTo: self.titleLB.bottomAnchor, constant: 8.0),
            nameInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            nameInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            mailInput.topAnchor.constraint(equalTo: self.nameInput.bottomAnchor, constant: 8.0),
            mailInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            mailInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            pwdInput.topAnchor.constraint(equalTo: self.mailInput.bottomAnchor, constant: 8.0),
            pwdInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            pwdInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            zonaInput.topAnchor.constraint(equalTo: self.pwdInput.bottomAnchor, constant: 8.0),
            zonaInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            zonaInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: self.zonaInput.bottomAnchor, constant: 8.0),
            saveButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            saveButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0)
        ])
        
    }
    
    
    func setAutolayoutInvited() {
        NSLayoutConstraint.activate([
            titleLB.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 32.0),
            titleLB.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            zonaInput.topAnchor.constraint(equalTo: self.titleLB.bottomAnchor, constant: 12.0),
            zonaInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            zonaInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            saveButton.topAnchor.constraint(equalTo: self.zonaInput.bottomAnchor, constant: 8.0),
            saveButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            saveButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0)
        ])
    }
    
    func setDataScreenProfile() {
            let currentUser = DataManager.shared.getDataUser()
            let apiResponse = ApiManager.shared.getInfoCurrentProfile(id: currentUser.flidentificadore7dbf413f9 ?? 0)
            guard let response = apiResponse.first else { return }
            nameInput.text = response.flnombre504267dd91
            mailInput.text = response.flcorreoelectronico57997cc6aa
            let zona = showData.filter({$0.flidentificador6d8056256d == response.flzona621bfe9801})
            guard let resulFilter = zona.first else { return }
            zonaInput.text = resulFilter.flzona0119225338
            zoneId = resulFilter.flidentificador6d8056256d ?? 0
        if flag == .register {
            nameInput.text = ""
            mailInput.text = ""
            zonaInput.text = ""
            pwdInput.text = ""
        }
    }
    
}

extension PerfilViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return showData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return showData[row].flzona0119225338
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let zona = showData[row].flzona0119225338 else { return }
        guard let iDzona = showData[row].flidentificador6d8056256d else { return }
        zonaInput.text = zona
        zoneId = iDzona
        self.view.endEditing(true)
    }

}

extension PerfilViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.pickerSelection.reloadInputViews()
            self.pickerSelection.reloadAllComponents()
        }
    }
}

