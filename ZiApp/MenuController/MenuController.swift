import Foundation
import UIKit
import SideMenuSwift

class MenuController: UIViewController {
    
    var delegateOptionMenu: optionMenu?
    var optionTitle: [String] = ["Mi perfil","Inicio","Favoritos","¿Quieres anunciar?","Cerrar sesión"]
    
    lazy var tableOfMenu: UITableView = {
        let properties = UITableView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
        properties.separatorStyle = .none
        return properties
    }()
    
    lazy var versionIndicator: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.font = UIFont(name: "Helvita-bold", size: 8)
        properties.textColor = primaryColorGreen
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.text = "Versión 1.0.0.0"
        return properties
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        if DataManager.shared.isInvited().0 {
            optionTitle.removeLast()
            optionTitle.append("Registrarse")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Menú"
        self.navigationController?.navigationBar.isHidden = true
        tableOfMenu.delegate = self
        tableOfMenu.dataSource = self
        
        self.view.addSubview(tableOfMenu)
        self.view.addSubview(versionIndicator)
        
        versionIndicator.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(changeValueText(_:)))
        versionIndicator.addGestureRecognizer(tapgesture)
        
        NSLayoutConstraint.activate([
            tableOfMenu.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 1.0),
            tableOfMenu.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            tableOfMenu.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            tableOfMenu.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            versionIndicator.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -1),
            versionIndicator.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 1),
            versionIndicator.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -1)
        ])
       
    }
    
    @objc func changeValueText(_ sender: UITapGestureRecognizer) {
        guard let currentText = versionIndicator.text else { return }
        let flagIndicator = currentText.contains("V")
        if flagIndicator { versionIndicator.text = baseUrl } else { versionIndicator.text = currentVersion }
    }
    
}

extension MenuController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        delegateOptionMenu?.changeScreen(option: indexPath.row)
    }
    
}

extension MenuController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableOfMenu.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        cell.textLabel?.text = optionTitle[indexPath.row]
        return cell
    }
    
}
