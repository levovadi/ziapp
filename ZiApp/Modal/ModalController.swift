
import Foundation
import UIKit

class ModalController: UIViewController {
    
    var options: [String] = ["¿Entrega en domicilio ?","¿Expide factura?","¿Pago con tarjeta?"]
    var Adcurrent: InfoAnunciosResponse?
    var arrayOptionselected: [Int] = []
 
    let interface: ModalView = {
        let interface = ModalView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.white
        interface.translatesAutoresizingMaskIntoConstraints = false
        interface.layer.cornerRadius = 10
        return interface
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.navigationController?.isNavigationBarHidden = true
        initComponents()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {}
    
    func getCommentsAndRating(id: Int?) {
        guard let searchId = id else { return }
        let summary = ApiManager.shared.summaryRatingAndCommentsWs(identificador: searchId)
        let comments = ApiManager.shared.getCommetsWs(id: searchId)
    }
    
    
    func loadBasicInformation(currentAd: DetailAdResponse) {
        let horarios = ApiManager.shared.getinfoanuncioWs(id: currentAd.flidentificador9a5668476f ?? 0)
        getCommentsAndRating(id: currentAd.flidentificador9a5668476f ?? 0)
        guard let horario = horarios.first else { return }
        let lunes = "Lunes:     \(horario.fllunesde9e99afbcb7 ?? "") - \(horario.fllunesa7be5f9030a ?? "")\n\n"
        let martes = "Martes:   \(horario.flmartesdec5fbff10df ?? "") - \(horario.flmartesa91f1957abc ?? "")\n\n"
        let miercoles = "Miércoles: \(horario.flmiercolesde7c98272aad ?? "") - \(horario.flmiercolesa02dcd0d36a ?? "")\n\n"
        let jueves = "Jueves:      \(horario.fljuevesde4074e90d99 ?? "") - \(horario.fljuevesad6e0ac5f12 ?? "")\n\n"
        let viernes = "Viernes:     \(horario.flviernesde5fee266186 ?? "") - \(horario.flviernesab7f6689767 ?? "")\n\n"
        let sabado = "Sábado:     \(horario.flsabadode15616d416d ?? "") - \(horario.flsabadoa23da90d617 ?? "")\n\n"
        let domingo = "Domingo:   \(horario.fldomingode45aefc131d ?? "") - \(horario.fldomingoa8273f920f2 ?? "")\n\n"
        Adcurrent = horario
        self.interface.subtitleLabel.text = lunes+martes+miercoles+jueves+viernes+sabado+domingo
        createOptionsArray()
    }
    
    func createOptionsArray() {
        guard let envio = Adcurrent?.flenviosa04ab1bf3a else { return }
        guard let factura = Adcurrent?.flfactura1621874f9b else { return }
        guard let credito = Adcurrent?.fltarjetadecredito73c161d82e else { return }
        arrayOptionselected.append(envio)
        arrayOptionselected.append(factura)
        arrayOptionselected.append(credito)
    }
    
    func modeRating() {
        self.interface.labelTitle.text = "Calificaciones"
        self.interface.subtitleLabel.text = "*****"
    }
    
    private func initComponents() {
        setSubviews()
        setAutolayout()
        actionsAndGestures()
        configureTableviewCell()
    }
    
    private func actionsAndGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapIcon))
        interface.iconEliminar.addGestureRecognizer(tap)
        interface.button.addTarget(self, action: #selector(cancelPressed), for: .touchUpInside)
    }
    
    @objc func tapIcon() {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func acceptPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func cancelPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    // Método para agregar las subvistas a la vista principal del controlador
    private func setSubviews() {
        view.addSubview(interface)
    }
    
    // Método para definir el autolayout de los componentes de la vista principal del controlador
    private func setAutolayout() {
        NSLayoutConstraint.activate([
            interface.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            interface.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            interface.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.95),
            interface.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.85)
            ])
    }
    
    func configureTableviewCell() {
        self.interface.radioOpcionTableMenu.delegate = self
        self.interface.radioOpcionTableMenu.dataSource = self
        self.interface.radioOpcionTableMenu.separatorStyle = .none
        self.interface.radioOpcionTableMenu.register(RadioTableViewCell.self, forCellReuseIdentifier: RadioTableViewCell.id)
        self.interface.radioOpcionTableMenu.rowHeight = UITableView.automaticDimension
        self.interface.radioOpcionTableMenu.allowsMultipleSelection = true
    }
    
}



extension ModalController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RadioTableViewCell.id, for: indexPath) as! RadioTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.optionTitle.text = options[indexPath.row]
        cell.optionTitle.backgroundColor = .clear
        cell.initSelection(value: arrayOptionselected[indexPath.row])
        cell.reloadInputViews()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
     func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        return indexPath
    }
    
     func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
    
}
