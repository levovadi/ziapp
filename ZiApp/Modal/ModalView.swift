import Foundation
import UIKit
import LTHRadioButton

class ModalView: UIView {
    
    var testRadioButton: LTHRadioButton = {
        let properties = LTHRadioButton(selectedColor: .red)
        properties.selectedColor = .red
        properties.selectedColor = .blue
        properties.translatesAutoresizingMaskIntoConstraints = false
        return properties
    }()
    
    var testRadioButton0: LTHRadioButton = {
        let properties = LTHRadioButton(selectedColor: .blue)
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.selectedColor = .red
        properties.selectedColor = .blue
        return properties
    }()
    
    var testRadioButton1: LTHRadioButton = {
        let properties = LTHRadioButton(selectedColor: .green)
        properties.selectedColor = .red
        properties.selectedColor = .blue
        properties.translatesAutoresizingMaskIntoConstraints = false
        return properties
    }()
    
    var stackRadioButton: UIStackView = {
        let properties = UIStackView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.axis = .vertical
        properties.distribution = .fillEqually
        properties.alignment = .center
        properties.backgroundColor = .orange
        properties.isUserInteractionEnabled = true
        return properties
    }()
    
    var iconEliminar: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "empty")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image?.withRenderingMode(.alwaysOriginal)
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    var labelSuperTitle: UILabel = {
        let label = UILabel()
        label.text = "Información del proveedor"
        label.backgroundColor = .clear
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = primaryColorGreen
        return label
    }()
    
    var labelTitle: UILabel = {
        let label = UILabel()
        label.text = "Horarios del vendedor"
        label.backgroundColor = .clear
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = UIColor.black
        return label
    }()
    
    var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.font = UIFont(name: "MyriadPro-Light", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .justified
        label.textColor = UIColor.darkGray
        label.sizeToFit()
        return label
    }()
    
    var button: UIButton = {
        let button = UIButton()
        button.setTitle("CERRAR", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.layer.borderWidth = 2.0
        button.layer.borderColor = primaryColorGreen.cgColor
        button.backgroundColor =  primaryColorGreen
        button.layer.cornerRadius = 10
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 18)
        return button
    }()
    
    var radioOpcionTableMenu: UITableView = {
        let tableMenu = UITableView()
        tableMenu.isScrollEnabled = true
        tableMenu.translatesAutoresizingMaskIntoConstraints = false
        tableMenu.backgroundColor = .white
        return tableMenu
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setView()
    }
    
    private func setView() {
        setSubviews()
        setAutolayout()
    }
    
    private func setSubviews() {
        addSubview(labelSuperTitle)
        addSubview(labelTitle)
        addSubview(subtitleLabel)
        addSubview(button)
        addSubview(radioOpcionTableMenu)
    }
    
    private func addtestRadioButton() {
        stackRadioButton.addArrangedSubview(testRadioButton)
        stackRadioButton.addArrangedSubview(testRadioButton0)
        stackRadioButton.addArrangedSubview(testRadioButton1)
        self.addSubview(stackRadioButton)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            labelSuperTitle.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor , constant: 8),
            labelSuperTitle.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            labelTitle.topAnchor.constraint(equalTo: self.labelSuperTitle.bottomAnchor , constant: 8),
            labelTitle.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: self.labelTitle.bottomAnchor , constant: 8),
            subtitleLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.90),
            subtitleLabel.leadingAnchor.constraint(equalTo: self.labelSuperTitle.leadingAnchor, constant: 1)
        ])
        
        NSLayoutConstraint.activate([
            radioOpcionTableMenu.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 0.0),
            radioOpcionTableMenu.leadingAnchor.constraint(equalTo: self.subtitleLabel.leadingAnchor, constant: 0),
            radioOpcionTableMenu.trailingAnchor.constraint(equalTo: self.subtitleLabel.trailingAnchor, constant: 0),
        ])
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: self.radioOpcionTableMenu.bottomAnchor, constant: 8),
            button.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1100),
            button.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.900),
            button.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor ,constant: -2)
        ])
        
    }
    
}
