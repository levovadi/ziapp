import Foundation
import UIKit
import LTHRadioButton

class RadioTableViewCell: UITableViewCell {
    
    static let id = "radioCellId"

    private let selectedColor   = UIColor(red: 74/255, green: 144/255, blue: 226/255, alpha: 1.0)
    private let deselectedColor = UIColor.lightGray
    
    var silabel: UILabel = {
        let optionTitle = UILabel()
        optionTitle.font = UIFont(name: "Helvetica-bold", size: 14)
        optionTitle.textAlignment = .center
        optionTitle.numberOfLines = 0
        optionTitle.text = "Sí"
        optionTitle.textColor = UIColor.black
        optionTitle.translatesAutoresizingMaskIntoConstraints = false
        optionTitle.backgroundColor = UIColor.clear
        return optionTitle
    }()
    
    var nolabel: UILabel = {
        let optionTitle = UILabel()
        optionTitle.font = UIFont(name: "Helvetica-bold", size: 14)
        optionTitle.textAlignment = .center
        optionTitle.numberOfLines = 0
        optionTitle.text = "NO"
        optionTitle.textColor = UIColor.black
        optionTitle.translatesAutoresizingMaskIntoConstraints = false
        optionTitle.backgroundColor = UIColor.clear
        return optionTitle
    }()
    
    var optionTitle: UILabel = {
        let optionTitle = UILabel()
        optionTitle.backgroundColor = .cyan
        optionTitle.font = UIFont(name: "Helvetica-light", size: 16)
        optionTitle.textAlignment = .justified
        optionTitle.text = "label example option"
        optionTitle.textColor = UIColor.black
        optionTitle.translatesAutoresizingMaskIntoConstraints = false
        optionTitle.backgroundColor = UIColor.clear
        optionTitle.numberOfLines = 2
        optionTitle.minimumScaleFactor = 8
        optionTitle.sizeToFit()
        return optionTitle
    }()
    
    var radioButtonYes: LTHRadioButton = {
        let icon = LTHRadioButton()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.selectedColor = .green
        icon.deselectedColor = .green
        return icon
    }()
    
    var radioButtonNo: LTHRadioButton = {
        let icon = LTHRadioButton()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.selectedColor = .green
        icon.deselectedColor = .green
        return icon
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(optionTitle)
        addSubview(radioButtonYes)
        addSubview(radioButtonNo)
        addSubview(silabel)
        addSubview(nolabel)
        setAutoLayout()
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setValue(value: Int) {
        
    }
    
    
    private func setAutoLayout() {
        NSLayoutConstraint.activate([
            optionTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            optionTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 1),
            optionTitle.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
           
            radioButtonYes.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            radioButtonYes.trailingAnchor.constraint(equalTo: self.radioButtonNo.leadingAnchor, constant: -8.0),
            radioButtonYes.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.07),
            radioButtonYes.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.07),
            radioButtonYes.centerYAnchor.constraint(equalTo: self.optionTitle.centerYAnchor, constant: 0.00),
            radioButtonYes.leadingAnchor.constraint(equalTo: self.optionTitle.trailingAnchor, constant: 24),
            
            radioButtonNo.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            radioButtonNo.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.07),
            radioButtonNo.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.07),
            radioButtonNo.centerYAnchor.constraint(equalTo: self.radioButtonYes.centerYAnchor, constant: 0.00),
            
            
            silabel.bottomAnchor.constraint(equalTo: radioButtonYes.topAnchor, constant: -1),
            silabel.centerXAnchor.constraint(equalTo: radioButtonYes.centerXAnchor, constant: 0),
            
            nolabel.bottomAnchor.constraint(equalTo: radioButtonNo.topAnchor, constant: -1),
            nolabel.centerXAnchor.constraint(equalTo: radioButtonNo.centerXAnchor, constant: 0)
            
        ])
    }
    
    func update(with color: UIColor) {
        backgroundColor             = color
        radioButtonYes.selectedColor   = color == .darkGray ? .white : selectedColor
        radioButtonYes.deselectedColor = color == .darkGray ? .lightGray : deselectedColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            radioButtonNo.deselect(animated: animated)
            return radioButtonYes.select(animated: animated)
        }
        
        radioButtonYes.deselect(animated: animated)
        radioButtonNo.select(animated: animated)
    }
    
    func initSelection(value: Int) {
        if value == 2 {
            radioButtonYes.deselect(animated: true)
            radioButtonNo.select(animated: true)
            setSelected(false, animated: true)
        } else {
            radioButtonNo.deselect(animated: true)
            radioButtonYes.select(animated: true)
            setSelected(true, animated: true)
        }
        self.reloadInputViews()
        self.layoutIfNeeded()
    }
    
}
