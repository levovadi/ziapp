import Foundation
import UIKit


class ModalRatingController: UIViewController {
    
    var commentsToShow: [CommentResponse] = []
    
 
    let interface: ModalRatingView = {
        let interface = ModalRatingView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.white
        interface.translatesAutoresizingMaskIntoConstraints = false
        interface.layer.cornerRadius = 10
        return interface
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.navigationController?.isNavigationBarHidden = true
        initComponents()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
    func loadBasicInformation(currentAd: DetailAdResponse) {
        commentsToShow = ApiManager.shared.getCommetsWs(id: currentAd.flidentificador9a5668476f ?? 0)
    }
    
     func loadFrame(identificador: Int) {
        let urlString = "http://6c646c5ab7a94fbfa6d3729edb265223.codium.mx/Comentariosapp/\(identificador)"
        guard let urlRequest = URL(string: urlString) else { return }
        self.interface.viewWeb.load(URLRequest(url: urlRequest))
    }
    
    private func initComponents() {
        setSubviews()
        setAutolayout()
        actionsAndGestures()
    }
    
    private func actionsAndGestures() {
        interface.button.addTarget(self, action: #selector(cancelPressed), for: .touchUpInside)
    }
    
    @objc func tapIcon() {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func acceptPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func cancelPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    // Método para agregar las subvistas a la vista principal del controlador
    private func setSubviews() {
        view.addSubview(interface)
    }
    
    // Método para definir el autolayout de los componentes de la vista principal del controlador
    private func setAutolayout() {
        NSLayoutConstraint.activate([
            interface.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            interface.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            interface.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.95),
            interface.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.85)
            ])
    }
    
}



extension ModalRatingController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.id, for: indexPath) as! CommentTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.setValues(values: commentsToShow[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
        
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(true, animated: true)
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        return indexPath
    }
    
}

