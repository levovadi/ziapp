import Foundation
import UIKit

class CommentTableViewCell: UITableViewCell {
    
    static let id = "commentCellId"
    
    var dateLB: UILabel = {
        let optionTitle = UILabel()
        optionTitle.font = UIFont(name: "Helvetica-bold", size: 14)
        optionTitle.textAlignment = .left
        optionTitle.numberOfLines = 0
        optionTitle.text = ""
        optionTitle.textColor = UIColor.black
        optionTitle.translatesAutoresizingMaskIntoConstraints = false
        optionTitle.backgroundColor = UIColor.clear
        return optionTitle
    }()
    
    var commentLB: UILabel = {
        let optionTitle = UILabel()
        optionTitle.font = UIFont(name: "Helvetica-bold", size: 14)
        optionTitle.textAlignment = .left
        optionTitle.numberOfLines = 0
        optionTitle.text = ""
        optionTitle.textColor = UIColor.black
        optionTitle.translatesAutoresizingMaskIntoConstraints = false
        optionTitle.backgroundColor = UIColor.clear
        return optionTitle
    }()
    
    var ratingLB: UILabel = {
        let optionTitle = UILabel()
        optionTitle.font = UIFont(name: "Helvetica-bold", size: 14)
        optionTitle.textAlignment = .left
        optionTitle.text = ""
        optionTitle.textColor = UIColor.black
        optionTitle.translatesAutoresizingMaskIntoConstraints = false
        optionTitle.backgroundColor = UIColor.clear
        optionTitle.numberOfLines = 0
        optionTitle.sizeToFit()
        return optionTitle
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(dateLB)
        addSubview(commentLB)
        addSubview(ratingLB)
        setAutoLayout()
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setValues(values: CommentResponse) {
        dateLB.text = "Fecha: \(values.flformatodefecha9081060b72 ?? "")"
        commentLB.text = "Comentario: \(values.flcomentariob330b9557e ?? "")"
        ratingLB.text = "Calificación: \(values.flcalificacion3fe0b68f3d ?? 0)"
    }
    
    
    private func setAutoLayout() {
        NSLayoutConstraint.activate([
            dateLB.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            dateLB.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            commentLB.topAnchor.constraint(equalTo: self.dateLB.bottomAnchor, constant: 8),
            commentLB.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            ratingLB.topAnchor.constraint(equalTo: self.commentLB.bottomAnchor, constant: 8),
            ratingLB.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            ratingLB.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }
    
}

