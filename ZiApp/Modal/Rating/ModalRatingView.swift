import Foundation
import UIKit
import WebKit
import SafariServices

class ModalRatingView: UIView {
    
    lazy var viewWeb: WKWebView = {
        let properties = WKWebView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .white
        return properties
    }()
    
    var button: UIButton = {
        let button = UIButton()
        button.setTitle("Cerrar", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.layer.borderWidth = 2.0
        button.layer.borderColor = primaryColorGreen.cgColor
        button.backgroundColor =  primaryColorGreen
        button.layer.cornerRadius = 10
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 18)
        return button
    }()
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setView()
    }
    
    private func setView() {
        setSubviews()
        setAutolayout()
    }
    
    private func setSubviews() {
        addSubview(viewWeb)
        addSubview(button)
    }
    
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            viewWeb.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            viewWeb.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            viewWeb.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            viewWeb.bottomAnchor.constraint(equalTo: self.button.topAnchor, constant: 0)
        ])
        

        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1100),
            button.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.900),
            button.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor ,constant: -2)
        ])
        
    }
    
}
