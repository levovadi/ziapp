import Foundation
import UIKit
import ImageSlideshow
import Kingfisher

protocol touchImage {
    func gotoDetail(id: Int?)
}

protocol reloadVistosRecientemente: AnyObject {
    func reloadData(with data: [String:String])
}

class DetailScreenController: UIViewController {
    
    var allAnunciosArray: [RecentAddedResponse] = []
    var titlesForSectionString: [String] = []
    let searchController = UISearchController(searchResultsController: nil)
    var hidebar: Bool = false
    lazy var searchBar = UISearchBar()
    var bannerArrayData: [BannerResponse] = []
    var currentIndexBanner: Int?

    lazy var scroll: UIScrollView = {
        let config = UIScrollView()
        config.translatesAutoresizingMaskIntoConstraints = false
        config.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        return config
    }()
    
    let localSource = [BundleImageSource(imageString: "img1"), BundleImageSource(imageString: "img2"), BundleImageSource(imageString: "img3"), BundleImageSource(imageString: "img4")]
    
    var categoryId: Int = 0
    var insideCategory: [RecentAddedResponse] = []
    var vistosRecientementeArray: [VistosRecientementeResponse] = []
    // var titlesArrayString: [String] = []
    var titlesForSections: [TitlesSectionResponse] = []
    
    var bannerSlideshow: ImageSlideshow = {
        let properties = ImageSlideshow()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.slideshowInterval = 5.0
        properties.contentScaleMode = .scaleToFill
        properties.pageIndicator = UIPageControl.withSlideshowColors()
        properties.activityIndicator = DefaultActivityIndicator()
        properties.zoomEnabled = false
        return properties
    }()
    
    let categoryCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(CellCarousel.self, forCellWithReuseIdentifier: CellCarousel.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = false
        listOptions.tag = 0
        listOptions.backgroundColor = UIColor(patternImage: UIImage(named: "background") ?? UIImage())
        return listOptions
    }()
    
    let todosLosAnunciosCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(CellCarousel.self, forCellWithReuseIdentifier: CellCarousel.id)
        listOptions.register(CellImageAndTitle.self, forCellWithReuseIdentifier: CellImageAndTitle.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = false
        listOptions.tag = 0
        listOptions.backgroundColor = UIColor.clear
        listOptions.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return listOptions
    }()
    
    var titleThirdSection: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "-"
        properties.textColor = primaryColorGreen
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 24)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.tabBarController?.navigationItem.titleView?.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background") ?? UIImage())
        self.title = "inicio"
        setSubviews()
        setAutolayout()
        configCollection()
        setNavItems()
        searchBar.placeholder = "Buscador"
        searchBar.delegate = self
        self.tabBarController?.navigationItem.titleView = searchBar
        bannerSlideshow.delegate = self
        tapgestureSlideshow()
    }
    
    func tapgestureSlideshow() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailScreenController.didTapBanner))
        bannerSlideshow.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func didTapBanner() {
        print("le diste clic en la seecion de hasta arriba")
        _ = ApiManager.shared.insertClic(adId: "\(bannerArrayData[currentIndexBanner ?? 0].flidentificador9a5668476f ?? 0)", userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .banner)
        let detail = ApiManager.shared.getAdDetailWs(identificador: bannerArrayData[currentIndexBanner ?? 0].flidentificador9a5668476f ?? 0)
        let nextVc = DetailDataImage()
        nextVc.setDetailData(detailData: detail)
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    func setSubviews() {
        self.view.addSubview(scroll)
        self.scroll.addSubview(bannerSlideshow)
        self.scroll.addSubview(categoryCollection)
        self.scroll.addSubview(titleThirdSection)
        self.scroll.addSubview(todosLosAnunciosCollection)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            scroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0),
            scroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            scroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            scroll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])
        NSLayoutConstraint.activate([
            bannerSlideshow.topAnchor.constraint(equalTo: self.scroll.topAnchor, constant: 0.0),
            bannerSlideshow.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.20),
            bannerSlideshow.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.00),
            bannerSlideshow.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            categoryCollection.topAnchor.constraint(equalTo: self.bannerSlideshow.safeAreaLayoutGuide.bottomAnchor, constant: 8.0),
            categoryCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.00),
            categoryCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.75),
            categoryCollection.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            titleThirdSection.topAnchor.constraint(equalTo: self.categoryCollection.bottomAnchor, constant: 0),
            titleThirdSection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
            titleThirdSection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.10),
            titleThirdSection.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            todosLosAnunciosCollection.topAnchor.constraint(equalTo: self.titleThirdSection.bottomAnchor,constant: 0),
            todosLosAnunciosCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
            todosLosAnunciosCollection.heightAnchor.constraint(equalToConstant: CGFloat(allAnunciosArray.count) * 0.32),
            //todosLosAnunciosCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.60),
            todosLosAnunciosCollection.bottomAnchor.constraint(equalTo: self.scroll.bottomAnchor, constant: 0.0)
        ])
    }
    
    func converToDictionary(identificador: Int) {
        let currentUser = DataManager.shared.getDataUser()
        categoryId = identificador
        titlesForSections.removeAll()
        titlesForSections = ApiManager.shared.getTitlesSections(id: identificador)
        setTitlesForSection(currentTitles: titlesForSections)
        vistosRecientementeArray.removeAll()
        insideCategory.removeAll()
        allAnunciosArray.removeAll()
        vistosRecientementeArray = ApiManager.shared.vistosRecientemente(profileId: currentUser.flidentificadore7dbf413f9 ?? 0, category: identificador, zoneId: currentUser.flzona621bfe9801 ?? 0)
        insideCategory = ApiManager.shared.recentlyAddedWs(category: identificador, zonaId: currentUser.flzona621bfe9801 ?? 0)
        allAnunciosArray = ApiManager.shared.getAllAnuncios(idCategory: categoryId, idZona: currentUser.flzona621bfe9801 ?? 0)
        var bannersUrl: [KingfisherSource] = []
        bannerArrayData = ApiManager.shared.getBannerWs(idCategory: categoryId,idZone: currentUser.flzona621bfe9801 ?? 0)
        for url in bannerArrayData {
            guard let urlPath = url.flmagenviewdb31471f8f else { return }
            guard let urlType = URL(string: baseUrl+urlPath) else { return }
            let kingsource = KingfisherSource(url: urlType)
            bannersUrl.append(kingsource)
        }
        bannerSlideshow.setImageInputs(bannersUrl)
        updateHeightCollection()
    }
    
    func updateHeightCollection() {
        NSLayoutConstraint.deactivate(todosLosAnunciosCollection.constraints)
        NSLayoutConstraint.activate([
            todosLosAnunciosCollection.topAnchor.constraint(equalTo: self.titleThirdSection.bottomAnchor,constant: 24),
            todosLosAnunciosCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
            todosLosAnunciosCollection.heightAnchor.constraint(equalToConstant: CGFloat(allAnunciosArray.count) * 255.00),
            todosLosAnunciosCollection.bottomAnchor.constraint(equalTo: self.scroll.bottomAnchor, constant: -60.0)
        ])
        todosLosAnunciosCollection.reloadData()
        todosLosAnunciosCollection.updateConstraints()
        todosLosAnunciosCollection.reloadInputViews()
        todosLosAnunciosCollection.layoutIfNeeded()
    }
    
    func setTitlesForSection(currentTitles: [TitlesSectionResponse]) {
        titlesForSectionString.removeAll()
        titlesForSectionString.append("Vistos recientemente")
        titlesForSectionString.append("Agregados recientemente")
        if let titles = currentTitles.first {
            titleThirdSection.text = titles.fltodoslosanunciospara6e6353758f ?? "Todos los anuncios"
        }
    }
    
    func setNavItems() {
        let searchIcon = UIImage(named: "3844432-24")
        let searchItem = UIBarButtonItem(image: searchIcon?.withRenderingMode(.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(searchAction))
        searchItem.tintColor = UIColor.white
        self.tabBarController?.navigationItem.rightBarButtonItem = searchItem
        navigationItem.searchController = nil
        searchController.searchBar.delegate = self
        self.definesPresentationContext = true
    }
    
    
    
    @objc func searchAction() {
        navigationItem.searchController = nil
        navigationItem.searchController?.searchBar.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.isHidden = false
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController?.hidesNavigationBarDuringPresentation = false
        if !hidebar {
            self.tabBarController?.navigationItem.titleView?.isHidden = false
        } else {
            self.tabBarController?.navigationItem.titleView?.isHidden = true
        }
        hidebar = !hidebar
    }
    
}

extension DetailScreenController: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        currentIndexBanner = page
    }
}

extension DetailScreenController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollection {
            return titlesForSectionString.count
        }
        if collectionView == todosLosAnunciosCollection {
            return allAnunciosArray.count
        }
    return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  CellCarousel.id, for: indexPath) as! CellCarousel
            cell.delegateTouchImage = self
            switch indexPath.row {
            case 0:
                cell.configureVistosRecientemente(current: vistosRecientementeArray)
                cell.queSeccionEs = "Vistos Recientemente"
            case 1:
                cell.configure(current: insideCategory)
                cell.queSeccionEs = "Agregados Recientemente"
            default:
                cell.configure(current: insideCategory)
                cell.queSeccionEs = "Todos los anuncios para algo"
            }
            cell.titleCategory.text = titlesForSectionString[indexPath.row]
            return cell
        }
        if collectionView == todosLosAnunciosCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  CellImageAndTitle.id, for: indexPath) as! CellImageAndTitle
            cell.delegateTouchImage = self
            cell.titleAnuncio.text = allAnunciosArray[indexPath.row].fltitulo83e9a8f656 ?? ""
            cell.currentItem = allAnunciosArray[indexPath.row]
            let path = allAnunciosArray[indexPath.row].flimagenpreview67d7dcb6a0 ?? ""
            let generalpath = baseUrl + path
            cell.imageAnuncio.kf.setImage(with: URL(string: generalpath))
            cell.queSeccionEs = ""
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt \(collectionView) \(indexPath)")
        if collectionView == categoryCollection {
            switch indexPath.row {
            case 0:
               print("case 0")
            case 1:
                print("case 1")
            default:
                print("default")
            }
        }
        if collectionView == todosLosAnunciosCollection {
           print("todos los anuncios")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wCell = view.frame.width * 0.95
        return CGSize(width: wCell, height: view.frame.height * 0.35)
    }
    
    func configCollection() {
        self.categoryCollection.delegate = self
        self.categoryCollection.dataSource = self
        self.todosLosAnunciosCollection.delegate = self
        self.todosLosAnunciosCollection.dataSource = self
    }
    
}

extension DetailScreenController: touchImage , reloadVistosRecientemente {
    
    func reloadData(with data: [String : String]) {
        self.categoryCollection.backgroundColor = .red
        guard let id = data["categoryId"] else { return }
        converToDictionary(identificador: Int(id) ?? 0)
    }
    
    func gotoDetail(id: Int?) {
        guard let idQuery = id else { return }
        let detail = ApiManager.shared.getAdDetailWs(identificador: idQuery)
        let nextVc = DetailDataImage()
        nextVc.delegate = self
        nextVc.setDetailData(detailData: detail)
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
}

extension DetailScreenController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let currentUser = DataManager.shared.getDataUser()
        if searchBar.text != "" &&  searchBar.text != " " {
            self.showloader()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let busqueda = ApiManager.shared.searchqueryWs(zonaId: currentUser.flzona621bfe9801 ?? 0, word: searchBar.text ?? "")
                self.hideloader()
                let searchScreen = SearchVC()
                searchScreen.modalPresentationStyle = .fullScreen
                searchScreen.searchArray = busqueda
                self.tabBarController?.navigationController?.pushViewController(searchScreen, animated: true)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        if searchBar.text != "" &&  searchBar.text != " " {
//            self.showloader()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                let busqueda = ApiManager.shared.searchqueryWs(word: searchBar.text ?? "")
//                self.hideloader()
//                let searchScreen = SearchVC()
//                searchScreen.modalPresentationStyle = .fullScreen
//                searchScreen.searchArray = busqueda
//                self.tabBarController?.navigationController?.pushViewController(searchScreen, animated: true)
//            }
//        }
    }
    
}

