import Foundation
import UIKit
import ImageSlideshow
import Kingfisher

class CellCarousel: UICollectionViewCell {
    
    var queSeccionEs: String = ""
    
    static let id = "idCellCarousel"
    var delegateTouchImage: touchImage?
    var currenIndex: Int?
    var currentArray: [RecentAddedResponse] = []
    var currentArrayVistosRecientemente: [VistosRecientementeResponse] = []
    
    let localSource = [BundleImageSource(imageString: "img1"), BundleImageSource(imageString: "img2"), BundleImageSource(imageString: "img3"), BundleImageSource(imageString: "img4")]
    
    var titleCategory: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Aqui va el texto del titulo de la categoria"
        properties.textColor = primaryColorGreen
        properties.sizeToFit()
        properties.textAlignment = .center
        properties.numberOfLines = 0
        properties.font = UIFont.systemFont(ofSize: 24)
        return properties
    }()
    
    var title: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Aqui va el texto del titulo o lo que sea"
        properties.textColor = .black
        properties.sizeToFit()
        properties.textAlignment = .center
        properties.numberOfLines = 0
        //properties.font = UIFont.systemFont(ofSize: 20)
        return properties
    }()
    
    var bottomLine: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .black
        return properties
    }()
    
    var slideshow: ImageSlideshow = {
        let properties = ImageSlideshow()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.slideshowInterval = 5.0
        properties.contentScaleMode = .scaleAspectFit
        properties.pageIndicator = UIPageControl.withSlideshowColors()
        properties.activityIndicator = DefaultActivityIndicator()
        properties.zoomEnabled = true
        return properties
    }()
    
    
    func configure(current: [RecentAddedResponse]) {
        currentArray = current
        var resultados: [KingfisherSource] = []
        for res in current {
            guard let path = res.flimagenpreview67d7dcb6a0 else { return }
            let completepath = baseUrl + path
            guard let urlImage = URL(string: completepath) else { return }
            let item = KingfisherSource(url: urlImage)
            resultados.append(item)
        }
        titleCategory.text = "Agregados recientemente"
        title.text = current.first?.fltitulo83e9a8f656
        slideshow.setImageInputs(resultados)
    }
    
    func configureVistosRecientemente(current: [VistosRecientementeResponse]) {
        currentArrayVistosRecientemente = current
        var resultados: [KingfisherSource] = []
        for res in current {
            guard let path = res.flimagenpreview67d7dcb6a0 else { return }
            let completepath = baseUrl + path
            guard let urlImage = URL(string: completepath) else { return }
            let item = KingfisherSource(url: urlImage)
            if resultados.count < 16 {
                resultados.append(item)
            }
        }
        // title.text = current.first?.fltitulo83e9a8f656
        slideshow.setImageInputs(resultados)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        addSubviews()
        setAutolayout()
        otherOptionSlideShow()
        tapGestureSlideshow()
    }
    
    func addSubviews() {
        self.addSubview(titleCategory)
        //self.addSubview(title)
        self.addSubview(slideshow)
        self.addSubview(bottomLine)
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            titleCategory.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 8.0),
            titleCategory.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleCategory.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
        
//        NSLayoutConstraint.activate([
//            title.topAnchor.constraint(equalTo: self.titleCategory.bottomAnchor, constant: 8.0),
//            title.leadingAnchor.constraint(equalTo: self.leadingAnchor),
//            title.trailingAnchor.constraint(equalTo: self.trailingAnchor)
//        ])
        
        NSLayoutConstraint.activate([
            slideshow.topAnchor.constraint(equalTo: self.titleCategory.safeAreaLayoutGuide.bottomAnchor, constant: 8.0),
            slideshow.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            slideshow.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            slideshow.bottomAnchor.constraint(equalTo: self.bottomLine.topAnchor, constant: -8)
        ])
        
        NSLayoutConstraint.activate([
            bottomLine.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.010),
            bottomLine.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.20),
            bottomLine.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0),
            bottomLine.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12.0)
        ])
        
    }
    
    func otherOptionSlideShow() {
        slideshow.delegate = self
        slideshow.setImageInputs(localSource)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    func tapGestureSlideshow() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideshow.addGestureRecognizer(gestureRecognizer)
    }
    

    @objc func didTap() {
        print("le diste clic en \(queSeccionEs)")
        print("!currentArray.isEmpty \(currentArray.isEmpty) \(!currentArray.isEmpty) \(currenIndex)")
        switch queSeccionEs {
        case "Vistos Recientemente":
            print("deberia entrar en vistos recienteme")
            if !currentArrayVistosRecientemente.isEmpty {
                print("y ejecutar esto")
                _ = ApiManager.shared.insertClic(adId: "\(currentArrayVistosRecientemente[currenIndex ?? 0].flidentificador9a5668476f ?? 0)", userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .recentlyViewed)
            }
        case "Agregados Recientemente":
            if !currentArray.isEmpty {
                _ = ApiManager.shared.insertClic(adId: "\(currentArray[currenIndex ?? 0].flidentificador9a5668476f ?? 0)", userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .recentlyAdded)
            }
        case "":
            if !currentArray.isEmpty {
                _ = ApiManager.shared.insertClic(adId: "\(currentArray[currenIndex ?? 0].flidentificador9a5668476f ?? 0)", userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .all)
            }
        default:
            print("default case")
        }
        
        if !currentArray.isEmpty {
            delegateTouchImage?.gotoDetail(id: currentArray[currenIndex ?? 0].flidentificador9a5668476f)
        }
        if !currentArrayVistosRecientemente.isEmpty {
            delegateTouchImage?.gotoDetail(id: currentArrayVistosRecientemente[currenIndex ?? 0].flidentificador9a5668476f)
        }
    }
    
}

extension CellCarousel: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        currenIndex = page
//        title.text = currentArray[page].fltitulo83e9a8f656
    }
}


extension UIPageControl {
    public var view: UIView {
        return self
    }

    public var page: Int {
        get {
            return currentPage
        }
        set {
            currentPage = newValue
        }
    }

    open override func sizeToFit() {
        var frame = self.frame
        frame.size = size(forNumberOfPages: numberOfPages)
        frame.size.height = 30
        self.frame = frame
    }

    public static func withSlideshowColors() -> UIPageControl {
        let pageControl = UIPageControl()

        if #available(iOS 13.0, *) {
            pageControl.currentPageIndicatorTintColor = UIColor { traits in
                traits.userInterfaceStyle == .dark ? .white : .lightGray
            }
        } else {
            pageControl.currentPageIndicatorTintColor = .lightGray
        }
        
        if #available(iOS 13.0, *) {
            pageControl.pageIndicatorTintColor = UIColor { traits in
                traits.userInterfaceStyle == .dark ? .systemGray : .black
            }
        } else {
            pageControl.pageIndicatorTintColor = .black
        }

        return pageControl
    }
}
