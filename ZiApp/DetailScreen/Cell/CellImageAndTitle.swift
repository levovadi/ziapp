

import Foundation
import UIKit

class CellImageAndTitle: UICollectionViewCell {
    
    var queSeccionEs: String = ""
    
    static let id = "idCellImageAndTitle"
    var delegateTouchImage: touchImage?
    var currentItem: RecentAddedResponse?
    
    var bottomLine: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .lightGray
        return properties
    }()
    
    var titleAnuncio: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Chaips's House"
        properties.textColor = .black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 18)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()

    let imageAnuncio: UIImageView = {
        let properties = UIImageView()
        properties.image = #imageLiteral(resourceName: "img3")
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        addSubviews()
        setAutolayout()
        tapGestureImage()
    }
    
    func tapGestureImage() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        imageAnuncio.isUserInteractionEnabled = true
        imageAnuncio.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func didTap() {
        if let currentItem = currentItem,
           let identificador = currentItem.flidentificador9a5668476f {
            _ = ApiManager.shared.insertClic(adId: "\(identificador)", userId: "\(DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)", type: .all)
            delegateTouchImage?.gotoDetail(id: currentItem.flidentificador9a5668476f ?? 0)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        self.addSubview(titleAnuncio)
        self.addSubview(imageAnuncio)
        self.addSubview(bottomLine)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            titleAnuncio.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleAnuncio.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            titleAnuncio.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        NSLayoutConstraint.activate([
            imageAnuncio.topAnchor.constraint(equalTo: self.titleAnuncio.bottomAnchor, constant: 0),
            imageAnuncio.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            imageAnuncio.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0),
            imageAnuncio.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -28)
        ])
        NSLayoutConstraint.activate([
            bottomLine.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.010),
            bottomLine.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.20),
            bottomLine.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0),
            bottomLine.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0)
        ])
    }
    
}
