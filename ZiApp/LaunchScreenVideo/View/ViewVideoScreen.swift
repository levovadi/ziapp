//
//  ViewVideoScreen.swift
//  ZiApp
//
//  Created by Mariana Méndez on 21/01/21.
//

import Foundation
import UIKit
import AVKit

class ViewVideoScreen: UIView,UIDelegateInterface {
    
    lazy var videoPlayer: AVPlayer = {
        guard let urlVideo = URL(string: "https://www.youtube.com/watch?v=mN_e5-fcGU4") else { return AVPlayer() }
        let config = AVPlayer(url: urlVideo)
        return config
    }()
    
    lazy var playerViewController: AVPlayerViewController = {
        let config = AVPlayerViewController()
        config.player = videoPlayer
        return config
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponents()
        self.backgroundColor = .blue
    }
    
    func initComponents() {
        addComponents()
        setAutolayout()
    }
    
    func addComponents() {
       
    }
    
    func setAutolayout() {
        
    }
    
}
