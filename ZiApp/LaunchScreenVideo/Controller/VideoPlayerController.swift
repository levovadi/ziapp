//
//  VideoPlayerController.swift
//  ZiApp
//
//  Created by Mariana Méndez on 21/01/21.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class VideoPlayerController: UIViewController {
    
    var contador: Int = 0
    
    lazy var videoPlayer: AVPlayer = {
        guard let urlVideo = URL(string: "https://www.youtube.com/watch?v=mN_e5-fcGU4") else { return AVPlayer() }
        let config = AVPlayer(url: urlVideo)
        return config
    }()
    
    lazy var playerViewController: AVPlayerViewController = {
        let config = AVPlayerViewController()
        config.player = videoPlayer
        return config
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
    }
    
    required init(UI: ViewVideoScreen) {
        super.init(nibName: nil, bundle: nil)
        self.view = UI
        self.view.backgroundColor = .black
        playVideo()
        self.loadViewIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        contador += 1
        if contador > 1 {
            self.view.backgroundColor = .black
        } else {
            playVideo()
            waitAndGoToMain()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: aqui agregar la validación para pasar directo a la pantalla del home
    private func waitAndGoToMain() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            self.dismiss(animated: true) {
                self.navigationController?.navigationBar.tintColor = UIColor.white
                // MARK: si ya esta logueado aqui consumir el servicio
                self.validateIfUserHasalReadyLogged()
                //self.navigationController?.setViewControllers([LoginController()], animated: true)
            }
        }
    }
    
    private func validateIfUserHasalReadyLogged() {
        let currentUser = DataManager.shared.getDataUser()
        if currentUser.flcorreoelectronico57997cc6aa != nil {
            let resultadoLogin = ApiManager.shared.loginWs(usermail: currentUser.flcorreoelectronico57997cc6aa ?? "", password: currentUser.flcontrasenaa8f7f67349 ?? "",isLogged: true)
            if resultadoLogin.count > 0 {
                self.navigationController?.setViewControllers([HomeCategory()], animated: true)
            }
        } else {
            self.navigationController?.setViewControllers([LoginController()], animated: true)
        }
    }
    
    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "intro", ofType:"mp4") else {
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.showsPlaybackControls = false
        playerController.player = player
        playerController.videoGravity = .resizeAspectFill
        player.rate = 2.0
        present(playerController, animated: true) {
            player.play()
        }
    }
    
}
