import Foundation
import UIKit

class PromotionsVC: UIViewController {
    
    var promotions: [PromotionResponse] = []
    
    var labelTitle: UILabel = {
        let label = UILabel()
        label.text = "Promociones"
        label.backgroundColor = .clear
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = primaryColorGreen
        return label
    }()
    
    let searchCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(CellFavorite.self, forCellWithReuseIdentifier: CellFavorite.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        return listOptions
    }()
    
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Promociones"
        self.view.addSubview(labelTitle)
        self.view.addSubview(searchCollection)
        self.searchCollection.delegate = self
        self.searchCollection.dataSource = self
        self.searchCollection.backgroundColor = .white
        setAutolayout()
    }
    
    func setTitleCategory(catId: Int) {
        let titles = ApiManager.shared.getTitlesSections(id: catId)
        self.labelTitle.text = titles.first?.flpromocionesparafc4293c5f6
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            labelTitle.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 18),
            labelTitle.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            labelTitle.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8),
            labelTitle.trailingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: -8),
        ])
        
        NSLayoutConstraint.activate([
            searchCollection.topAnchor.constraint(equalTo: self.labelTitle.bottomAnchor, constant: 8),
            searchCollection.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            searchCollection.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            searchCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])
    }
    
}


extension PromotionsVC: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  CellFavorite.id, for: indexPath) as! CellFavorite
        cell.imageFavorite.isUserInteractionEnabled = false
        cell.configurePromotions(current: promotions[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selection = promotions[indexPath.item].flanuncioa653fd3644 ?? 0
        let detail = ApiManager.shared.getAdDetailWs(identificador: selection)
        let nextVc = DetailDataImage()
        nextVc.setDetailData(detailData: detail)
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wCell = view.frame.width * 1.0
        let hCell = view.frame.height * 0.30
        return CGSize(width: wCell, height: hCell)
    }
    
}
