
import UIKit
import Foundation
import ImageSlideshow


class CellSearchCollection: UICollectionViewCell {
    
    static let id = "idCellSearchCollection"
    
    var currentAdObject: searchResponse?
    var delegate: tapSlideshow?
    
    var slideshow: ImageSlideshow = {
        let properties = ImageSlideshow()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.slideshowInterval = 5.0
        properties.contentScaleMode = .scaleAspectFit
        properties.pageIndicator = UIPageControl.withSlideshowColors()
        properties.activityIndicator = DefaultActivityIndicator()
        properties.zoomEnabled = true
        properties.backgroundColor = .clear
        properties.isUserInteractionEnabled = true
        return properties
    }()

    var titleAd: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = ""
        properties.textColor = primaryColorGreen
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 19)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var containerTitle: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .clear
        return properties
    }()
    
    var horizontalStack: UIStackView = {
        let properties = UIStackView()
        properties.backgroundColor = .clear
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.distribution = .fillEqually
        properties.axis = .horizontal
        properties.alignment = .fill
        return properties
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        addSubviews()
        addgestureImages()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(current: searchResponse) {
        currentAdObject = current
        guard let path = current.flimagenpreview67d7dcb6a0 else { return }
        let generalpath = baseUrl + path
        guard let url = URL(string: generalpath) else { return }
        slideshow.setImageInputs([KingfisherSource(url: url)])
        titleAd.text = current.fltitulo83e9a8f656
        setAutolayout()
    }
    
    func addgestureImages() {
        let gestureToZoomImage = UITapGestureRecognizer(target: self, action: #selector(CellSearchCollection.didTapToZoomImage))
        slideshow.addGestureRecognizer(gestureToZoomImage)
    }

    @objc func didTapToZoomImage() {
        delegate?.showFullscreen(currentCell: self)
    }
    
    func addSubviews() {
        self.horizontalStack.addArrangedSubview(slideshow)
        self.horizontalStack.addArrangedSubview(containerTitle)
        self.containerTitle.addSubview(titleAd)
        self.addSubview(horizontalStack)
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            horizontalStack.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 8.0),
            horizontalStack.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 8.0),
            horizontalStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0),
            horizontalStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0),
        ])
        
        NSLayoutConstraint.activate([
            self.titleAd.centerYAnchor.constraint(equalTo: self.containerTitle.centerYAnchor, constant: -19.5),
            self.titleAd.leadingAnchor.constraint(equalTo: self.containerTitle.leadingAnchor, constant: 0),
            self.titleAd.trailingAnchor.constraint(equalTo: self.containerTitle.trailingAnchor, constant: 0),
            self.titleAd.heightAnchor.constraint(equalTo: self.containerTitle.heightAnchor, multiplier: 0.50)
        ])
        
    }


}
