import Foundation
import UIKit

protocol tapSlideshow {
    func showFullscreen(currentCell: CellSearchCollection)
}

class SearchVC: UIViewController {
    
    var searchArray: [searchResponse] = []
    var searchSubCategoryArray: [RecentAddedResponse] = []
    var promotions: [PromotionResponse] = []
    var delegate: reloadSubcategories?
    var currentIndex: IndexPath = IndexPath(item: 0, section: 0)
    var parentCategory: Int = 0
    
    var arrayBottomTabImages: [String] = ["img10932","img10934","img10936","img10938","img10940"]
    var arraySelectedTabImages: [String] = ["img10933","img10935","img10937","img10939","img10941"]
    
    lazy var indicatorBottomCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(ImageCategoryCell.self, forCellWithReuseIdentifier: ImageCategoryCell.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        listOptions.tag = 1
        listOptions.isHidden = true
        return listOptions
    }()
    
    lazy var titleSearchControllerLabel: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.textColor = UIColor.black
        properties.font = UIFont(name: "Helvetica-bold", size: 20)
        properties.numberOfLines = 0
        properties.text = "Resultados de búsqueda: "
        properties.textAlignment = .center
        return properties
    }()
    
    let searchCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(CellFavorite.self, forCellWithReuseIdentifier: CellFavorite.id)
        listOptions.register(CellSearchCollection.self, forCellWithReuseIdentifier: CellSearchCollection.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        return listOptions
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        if searchArray.count == 0 && searchSubCategoryArray.count == 0 && promotions.count == 0{
            titleSearchControllerLabel.text = "No se encontraron resultados: "
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.reloadCollection(catId: parentCategory)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Búsqueda"
        self.view.addSubview(titleSearchControllerLabel)
        self.view.addSubview(searchCollection)
        self.view.addSubview(indicatorBottomCollection)
        self.searchCollection.delegate = self
        self.searchCollection.dataSource = self
        self.searchCollection.backgroundColor = .white
        setAutolayout()
        setNavsItems()
        tabmenuCollectionConfiguration()
    }
    
    func tabmenuCollectionConfiguration() {
        indicatorBottomCollection.delegate = self
        indicatorBottomCollection.dataSource = self
    }
    
    func setNavsItems() {
        self.title = "Buscador"
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            titleSearchControllerLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 18),
            titleSearchControllerLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            searchCollection.topAnchor.constraint(equalTo: self.titleSearchControllerLabel.bottomAnchor, constant: 8),
            searchCollection.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            searchCollection.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            searchCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        ])
        NSLayoutConstraint.activate([
            indicatorBottomCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            indicatorBottomCollection.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0),
            indicatorBottomCollection.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.95),
            indicatorBottomCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15)
        ])
    }
    
}

extension SearchVC: tapSlideshow {
    func showFullscreen(currentCell: CellSearchCollection) {
        currentCell.slideshow.presentFullScreenController(from: self)
    }
}

extension SearchVC: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == indicatorBottomCollection {
            return arrayBottomTabImages.count
        }
        if searchArray.count > 0 {
            return searchArray.count
        } else {
            self.titleSearchControllerLabel.textColor = primaryColorGreen
            return searchSubCategoryArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == indicatorBottomCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  ImageCategoryCell.id, for: indexPath) as! ImageCategoryCell
            cell.backgroundColor = .clear
            cell.imageCatalog.backgroundColor = .clear
            cell.imageCatalog.image = UIImage(named: arrayBottomTabImages[indexPath.row])
            return cell
        }
        if searchArray.count > 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  CellSearchCollection.id, for: indexPath) as! CellSearchCollection
            cell.delegate = self
            cell.configure(current: searchArray[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  CellFavorite.id, for: indexPath) as! CellFavorite
            cell.imageFavorite.isUserInteractionEnabled = false
            cell.congigureSearchSubCategory(current: searchSubCategoryArray[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == indicatorBottomCollection {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCategoryCell {
                cell.backgroundColor = .clear
                cell.imageCatalog.backgroundColor = .clear
                cell.imageCatalog.image = UIImage(named: arraySelectedTabImages[indexPath.row])
                toggleOption(option: indexPath.row)
            }
        }
        
        if collectionView == searchCollection {
            if let cell = collectionView.cellForItem(at: indexPath) as? CellSearchCollection {
                let detail = ApiManager.shared.getAdDetailWs(identificador: searchArray[indexPath.row].flidentificador9a5668476f ?? 0)
                let nextVc = DetailDataImage()
                nextVc.setDetailData(detailData: detail)
                self.navigationController?.pushViewController(nextVc, animated: true)
            }
            if let cell = collectionView.cellForItem(at: indexPath) as? CellFavorite {
                let detail = ApiManager.shared.getAdDetailWs(identificador: searchSubCategoryArray[indexPath.row].flidentificador9a5668476f ?? 0)
                let nextVc = DetailDataImage()
                nextVc.setDetailData(detailData: detail)
                self.navigationController?.pushViewController(nextVc, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == indicatorBottomCollection {
            if let cell = collectionView.cellForItem(at: indexPath) as? ImageCategoryCell {
                cell.backgroundColor = .clear
                cell.imageCatalog.backgroundColor = .clear
                cell.imageCatalog.image = UIImage(named: arrayBottomTabImages[indexPath.row])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == indicatorBottomCollection {
            let wCell = view.frame.height * 0.10
            let hCell = view.frame.height * 0.10
            return CGSize(width: wCell, height: hCell)
        }
        let wCell = view.frame.width * 1.0
        let hCell = view.frame.height * 0.30
        return CGSize(width: wCell, height: hCell)
    }
    
    func toggleOption(option: Int) {
        switch option {
        case 0:
            self.navigationController?.popViewController(animated: true)
        case 1:
            print("[option] [value] [\(option)]")
        case 2:
            print("[option] [value] [\(option)]")
        case 3:
            print("[option] [value] [\(option)]")
        case 4:
            print("[option] [value] [\(option)]")
        case 5:
            print("[option] [value] [\(option)]")
        default:
            print("do something")
        }
    }
    
}
