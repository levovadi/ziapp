//
//  QuieresAnunciarController.swift
//  ZiApp
//
//  Created by Mariana Méndez on 28/01/21.
//

//import Foundation
//import UIKit
//import WebKit
//
//class RegistrationWebController: UIViewController {
//
//    var urlString =  "http://ziweb6c646c5223.codium.mx/Registrousuarios/0"
//
//    lazy var viewWeb: WKWebView = {
//        let properties = WKWebView()
//        properties.translatesAutoresizingMaskIntoConstraints = false
//        return properties
//    }()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.view.backgroundColor = .white
//        addSubviews()
//        setAutolayout()
//        guard let urlRequest = URL(string: urlString) else { return }
//        viewWeb.load(URLRequest(url: urlRequest))
//    }
//
//    func addSubviews() {
//        self.view.addSubview(viewWeb)
//    }
//
//    func setAutolayout() {
//        NSLayoutConstraint.activate([
//            viewWeb.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0),
//            viewWeb.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
//            viewWeb.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
//            viewWeb.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
//        ])
//    }
//
//}
