import Foundation
import UIKit

protocol touchImageFavorite {
    func removeFromFavorite(identificador: Int?)
}

class FavoritesController: UIViewController {
    
    var favoritesArray = ApiManager.shared.favoritesWs(idUser: DataManager.shared.getDataUser().flidentificadore7dbf413f9 ?? 0)
    
    lazy var messageRegister: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Favor de crear su perfil para guardar sus favoritos."
        properties.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        properties.textColor = primaryColorGreen
        properties.numberOfLines = 0
        properties.textAlignment = .justified
        return properties
    }()
    
    lazy var registerButton: UIButton = {
        let properties = UIButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setTitle("Registrarse", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.setTitleColor(.white, for: .normal)
        properties.titleLabel?.textColor = .white
        properties.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        return properties
    }()
    
    lazy var bottomMessageRegisterView: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.85)
        return properties
    }()
    
    lazy var titleLabel: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.textColor = primaryColorGreen
        properties.font = UIFont(name: "Helvetica-bold", size: 20)
        properties.numberOfLines = 0
        properties.text = "Favoritos"
        properties.textAlignment = .center
        return properties
    }()
    
    let favoriteCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 5
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(CellFavorite.self, forCellWithReuseIdentifier: CellFavorite.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        return listOptions
    }()
    
    var horizontalStack: UIStackView = {
        let properties = UIStackView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.axis = .horizontal
        properties.backgroundColor = .clear
        properties.distribution = .fillEqually
        properties.spacing = 4.0
        properties.isUserInteractionEnabled = true
        properties.isMultipleTouchEnabled = true
        return properties
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Favoritos"
        self.view.addSubview(titleLabel)
        self.view.addSubview(favoriteCollection)
        self.favoriteCollection.delegate = self
        self.favoriteCollection.dataSource = self
        self.favoriteCollection.backgroundColor = .white
        setAutolayout()
        if DataManager.shared.getFlagFirstEntry() {
            setModalUserInvited()
        }
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            self.titleLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 18),
            self.titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            favoriteCollection.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 8),
            favoriteCollection.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            favoriteCollection.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            favoriteCollection.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -UIScreen.main.bounds.height * 0.10)
        ])
        
    }
    
    func addTargetRegisterButton() {
        registerButton.addTarget(self, action: #selector(registerToogle), for: .touchUpInside)
    }
    
    @objc func registerToogle() {
        let nexvc = PerfilViewController()
        nexvc.flag = .register
        self.navigationController?.pushViewController(nexvc, animated: true)
    }
    
    func setModalUserInvited() {
        addTargetRegisterButton()
        self.view.addSubview(bottomMessageRegisterView)
        self.bottomMessageRegisterView.addSubview(messageRegister)
        self.bottomMessageRegisterView.addSubview(registerButton)
        
        NSLayoutConstraint.activate([
            bottomMessageRegisterView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.80),
            bottomMessageRegisterView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0),
            bottomMessageRegisterView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            bottomMessageRegisterView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            messageRegister.topAnchor.constraint(equalTo: self.bottomMessageRegisterView.topAnchor, constant: 8.0),
            messageRegister.leadingAnchor.constraint(equalTo: self.bottomMessageRegisterView.leadingAnchor, constant: 8),
            messageRegister.trailingAnchor.constraint(equalTo: self.bottomMessageRegisterView.trailingAnchor, constant: -8),
            messageRegister.bottomAnchor.constraint(equalTo: self.registerButton.topAnchor, constant: 8),
        ])
        
        NSLayoutConstraint.activate([
            registerButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.70),
            registerButton.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.05),
            registerButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            registerButton.bottomAnchor.constraint(equalTo: self.bottomMessageRegisterView.bottomAnchor, constant: -14.0)
        ])
    }
    
    
}

extension FavoritesController: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favoritesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  CellFavorite.id, for: indexPath) as! CellFavorite
        cell.configure(current: favoritesArray[indexPath.row])
        cell.delegateNotif = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selection = favoritesArray[indexPath.item].flidentificador9a5668476f ?? 0
        let detail = ApiManager.shared.getAdDetailWs(identificador: selection)
        let nextVc = DetailDataImage()
        nextVc.setDetailData(detailData: detail)
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wCell = view.frame.width * 1.0
        let hCell = view.frame.height * 0.30
        return CGSize(width: wCell, height: hCell)
    }
    
}

extension FavoritesController: touchImageFavorite {
    
    func removeFromFavorite(identificador: Int?) {
        let alerta = UIAlertController(title: "", message: "¿Estás seguro de borrar de favoritos?", preferredStyle: .alert)
        let accion = UIAlertAction(title: "Aceptar", style: .default) { (acc) in
            let currentUser = DataManager.shared.getDataUser()
            let _ = ApiManager.shared.removeFromFavorites(identificador: identificador ?? 0, profileIdentificador: currentUser.flidentificadore7dbf413f9 ?? 0)
            self.favoritesArray = ApiManager.shared.favoritesWs(idUser: currentUser.flidentificadore7dbf413f9 ?? 0)
            self.favoriteCollection.reloadData()
            self.favoriteCollection.reloadInputViews()
        }
        let action2 = UIAlertAction(title: "Cancelar", style: .default) { (acc) in
        }
        alerta.addAction(accion)
        alerta.addAction(action2)
        self.navigationController?.present(alerta, animated: true, completion: nil)
    }
    
}
