import Foundation
import UIKit
import Kingfisher

enum typeCellIndicator {
    case search
    case favorite
    case promotion
    case searchSubCategory
}

class CellFavorite: UICollectionViewCell {
    
    static let id = "idCellFavorite"
    var currentFavoriteObject: FavoriteResponse?
    var delegateNotif: touchImageFavorite?
    var typeCellFlag: typeCellIndicator = .favorite
    
    let imageFavorite: UIImageView = {
        let properties = UIImageView()
        properties.image = #imageLiteral(resourceName: "placeholderImage")
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var iconFavorite: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "checked")
        properties.contentMode = .scaleAspectFit
        properties.backgroundColor = .clear
        return properties
    }()
    
    var titlefavoriteLB: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = ""
        properties.textColor = .black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 15)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    var verticalStack: UIStackView = {
        let properties = UIStackView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.axis = .vertical
        properties.distribution = .fillProportionally
        return properties
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        addSubviews()
        addgestureImages()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(current: FavoriteResponse) {
        currentFavoriteObject = current
        guard let path = current.flimagenpreview67d7dcb6a0 else { return }
        let generalpath = baseUrl + path
        imageFavorite.kf.setImage(with: URL(string: generalpath))
        titlefavoriteLB.text = current.fltitulo83e9a8f656
        setAutolayoutForFavorite()
    }
    
    func configureForSearchController(current: searchResponse) {
        guard let path = current.flimagenpreview67d7dcb6a0 else { return }
        let generalpath = baseUrl + path
        imageFavorite.kf.setImage(with: URL(string: generalpath))
        titlefavoriteLB.text = current.fltitulo83e9a8f656
        iconFavorite.isHidden = true
        titlefavoriteLB.textColor = UIColor.black
        titlefavoriteLB.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        setAutolayoutForSearchQuery()
    }
    
    func configurePromotions(current: PromotionResponse) {
        guard let path = current.flimagenviewebbb866cf0 else { return }
        let generalpath = baseUrl + path
        imageFavorite.kf.setImage(with: URL(string: generalpath))
        titlefavoriteLB.text = current.fltitulopromocionbe71cf67ac
        iconFavorite.isHidden = true
        imageFavorite.backgroundColor = .clear
        titlefavoriteLB.backgroundColor = .clear
        titlefavoriteLB.textAlignment = .left
        titlefavoriteLB.numberOfLines = 0
        setAutolayoutPromotions()
    }
    
    func congigureSearchSubCategory(current: RecentAddedResponse) {
        guard let path = current.flimagenpreview67d7dcb6a0 else { return }
        let generalpath = baseUrl + path
        imageFavorite.kf.setImage(with: URL(string: generalpath))
        titlefavoriteLB.font = .systemFont(ofSize: 20, weight: .bold)
        titlefavoriteLB.text = current.fltitulo83e9a8f656
        titlefavoriteLB.textColor = UIColor.black
        titlefavoriteLB.backgroundColor = .clear
        imageFavorite.backgroundColor = .clear
        iconFavorite.isHidden = true
        setAutolayoutsubcategory()
        self.reloadInputViews()
        self.updateConstraints()
    }
    
    func addgestureImages() {
        let gesture0 = UITapGestureRecognizer(target: self, action: #selector(actionFavoriteDetail(tapGestureRecognizer:)))
        imageFavorite.isUserInteractionEnabled = true
        imageFavorite.addGestureRecognizer(gesture0)
        
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(actionFavoriteRemove(tapGestureRecognizer:)))
        iconFavorite.isUserInteractionEnabled = true
        iconFavorite.addGestureRecognizer(gesture1)
    }
    
    @objc func actionFavoriteDetail(tapGestureRecognizer: UITapGestureRecognizer) {
        print("[actionFavoriteDetail] \(currentFavoriteObject?.flidentificador9a5668476f ?? 0)")
    }
    
    @objc func actionFavoriteRemove(tapGestureRecognizer: UITapGestureRecognizer) {
        print("[actionFavoriteRemove] \(currentFavoriteObject?.flidentificador9a5668476f ?? 0)")
        delegateNotif?.removeFromFavorite(identificador: currentFavoriteObject?.flidentificador9a5668476f)
    }
    
    func addSubviews() {
        self.addSubview(imageFavorite)
        self.addSubview(titlefavoriteLB)
        self.addSubview(iconFavorite)
    }
    
    func setAutolayoutForSearchQuery() {
        NSLayoutConstraint.activate([
            imageFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.90),
            imageFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.65),
            imageFavorite.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
            imageFavorite.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        NSLayoutConstraint.activate([
            titlefavoriteLB.leadingAnchor.constraint(equalTo: self.imageFavorite.trailingAnchor, constant: 8),
            //titlefavoriteLB.topAnchor.constraint(equalTo: self.topAnchor, constant: 80.0),
            titlefavoriteLB.centerYAnchor.constraint(equalTo: self.imageFavorite.centerYAnchor, constant: 0),
            titlefavoriteLB.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0)
        ])
        NSLayoutConstraint.activate([
            iconFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.10),
            iconFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.10),
            iconFavorite.topAnchor.constraint(equalTo: self.titlefavoriteLB.bottomAnchor, constant: 8.0),
            iconFavorite.centerXAnchor.constraint(equalTo: self.titlefavoriteLB.centerXAnchor, constant: 0)
        ])
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            imageFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.90),
            imageFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.65),
            imageFavorite.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
            imageFavorite.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        
        NSLayoutConstraint.activate([
            titlefavoriteLB.leadingAnchor.constraint(equalTo: self.imageFavorite.trailingAnchor, constant: 1),
            titlefavoriteLB.topAnchor.constraint(equalTo: self.imageFavorite.topAnchor, constant: 8.0),
            titlefavoriteLB.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0)
        ])
        
        NSLayoutConstraint.activate([
            iconFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.10),
            iconFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.10),
            iconFavorite.topAnchor.constraint(equalTo: self.titlefavoriteLB.bottomAnchor, constant: 8.0),
            iconFavorite.centerXAnchor.constraint(equalTo: self.titlefavoriteLB.centerXAnchor, constant: 0)
        ])
        
    }
    
    func setAutolayoutPromotions() {
        
        NSLayoutConstraint.activate([
            imageFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.90),
            imageFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.65),
            imageFavorite.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
            imageFavorite.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        
        NSLayoutConstraint.activate([
            titlefavoriteLB.leadingAnchor.constraint(equalTo: self.imageFavorite.trailingAnchor, constant: 1),
            titlefavoriteLB.topAnchor.constraint(equalTo: self.imageFavorite.topAnchor, constant: 1.0),
            titlefavoriteLB.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0.0)
        ])
        
        NSLayoutConstraint.activate([
            iconFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.10),
            iconFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.10),
            iconFavorite.topAnchor.constraint(equalTo: self.titlefavoriteLB.bottomAnchor, constant: 8.0),
            iconFavorite.centerXAnchor.constraint(equalTo: self.titlefavoriteLB.centerXAnchor, constant: 0)
        ])
        
    }
    
    func setAutolayoutForFavorite() {
        
        NSLayoutConstraint.activate([
            imageFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.90),
            imageFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.55),
            imageFavorite.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
            imageFavorite.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
        ])
        
        NSLayoutConstraint.activate([
            iconFavorite.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.15),
            iconFavorite.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.15),
            iconFavorite.topAnchor.constraint(equalTo: self.topAnchor, constant: 50.0),
            iconFavorite.centerXAnchor.constraint(equalTo: self.titlefavoriteLB.centerXAnchor, constant: 0)
        ])
        
        NSLayoutConstraint.activate([
            titlefavoriteLB.topAnchor.constraint(equalTo: self.iconFavorite.bottomAnchor, constant: 8.0),
            titlefavoriteLB.leadingAnchor.constraint(equalTo: self.imageFavorite.trailingAnchor, constant: 8),
            titlefavoriteLB.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0)
        ])
        
    }
    
    func setAutolayoutsubcategory() {
        NSLayoutConstraint.deactivate(titlefavoriteLB.constraints)
        NSLayoutConstraint.deactivate(imageFavorite.constraints)
        iconFavorite.removeFromSuperview()
        
        NSLayoutConstraint.activate([
            titlefavoriteLB.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 18.0),
            titlefavoriteLB.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            titlefavoriteLB.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0),
        ])
        
        NSLayoutConstraint.activate([
            imageFavorite.topAnchor.constraint(equalTo: self.titlefavoriteLB.bottomAnchor, constant: 18.0),
            imageFavorite.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            imageFavorite.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
        
    }
    
}
