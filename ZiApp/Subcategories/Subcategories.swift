import Foundation
import UIKit

protocol reloadSubcategories {
    func reloadCollection(catId: Int)
}

class SubcategoriesController: UIViewController {
    
    var parentCategory: Int? = DataManager.shared.loadCurrentCategory()
    var subcategoriesArray: [subCategoryResponse] = []
    
    
    var titleScreenLB: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "Categorias para comida"
        properties.textColor = primaryColorGreen
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 24)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    let subcategoriesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 10
        let listOptions = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        listOptions.register(SubCategoryCell.self, forCellWithReuseIdentifier: SubCategoryCell.id)
        listOptions.translatesAutoresizingMaskIntoConstraints = false
        listOptions.backgroundColor = UIColor.white.withAlphaComponent(0)
        listOptions.backgroundColor = UIColor.clear
        listOptions.isScrollEnabled = true
        return listOptions
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        subcategoriesArray = ApiManager.shared.getSubcategories(identificador: parentCategory ?? 99)
        subcategoriesArray.sort {
            $0.flsubcategoria4d0825099c ?? "" < $1.flsubcategoria4d0825099c ?? ""
        }
        setTitleCategory(catId: parentCategory ?? 99)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        subcategoriesArray = ApiManager.shared.getSubcategories(identificador: parentCategory ?? 99)
        subcategoriesArray.sort {
            $0.flsubcategoria4d0825099c ?? "" < $1.flsubcategoria4d0825099c ?? ""
        }
        setTitleCategory(catId: parentCategory ?? 99)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        addSubviews()
        setAutolayout()
        delegatesColletion()
    }
    
    func setTitleCategory(catId: Int) {
        let titles = ApiManager.shared.getTitlesSections(id: catId)
        self.titleScreenLB.text = titles.first?.flcategoriaspara90c8d88493
    }
    
    func configureData() {
        subcategoriesArray = ApiManager.shared.getSubcategories(identificador: parentCategory ?? 0)
        subcategoriesArray.sort {
            $0.flsubcategoria4d0825099c ?? "" < $1.flsubcategoria4d0825099c ?? ""
        }
        subcategoriesCollection.reloadData()
        subcategoriesCollection.reloadInputViews()
    }
    
    func addSubviews() {
        self.view.addSubview(titleScreenLB)
        self.view.addSubview(subcategoriesCollection)
    }
    
    func removeViews() {
        titleScreenLB.removeFromSuperview()
        subcategoriesCollection.removeFromSuperview()
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            titleScreenLB.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8.0),
            titleScreenLB.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.10),
            titleScreenLB.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        NSLayoutConstraint.activate([
            subcategoriesCollection.topAnchor.constraint(equalTo: self.titleScreenLB.bottomAnchor, constant: 0),
            subcategoriesCollection.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8),
            subcategoriesCollection.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8),
            subcategoriesCollection.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.65)
        ])
    }
    
    func delegatesColletion() {
        subcategoriesCollection.delegate = self
        subcategoriesCollection.dataSource = self
    }
    
}

extension SubcategoriesController: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subcategoriesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  SubCategoryCell.id, for: indexPath) as! SubCategoryCell
        cell.configure(currentId: subcategoriesArray[indexPath.row].flidentificadorb9c0b0d58c ?? 0)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let searchVC = SearchVC()
        searchVC.searchSubCategoryArray = ApiManager.shared.searchAnuncioBySubcategory(idCategory: parentCategory ?? 0, idSubcategory: subcategoriesArray[indexPath.row].flidentificadorb9c0b0d58c ?? 0)
        searchVC.modalPresentationStyle = .fullScreen
        searchVC.delegate = self
        searchVC.parentCategory = parentCategory ?? 0
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let wCell = view.frame.width * 1.0
        let hCell = view.frame.height * 0.08
        return CGSize(width: wCell, height: hCell)
    }
    
}

extension SubcategoriesController: reloadSubcategories {
    func reloadCollection(catId: Int) {
        parentCategory = catId
        subcategoriesArray = ApiManager.shared.getSubcategories(identificador: catId)
        subcategoriesArray.sort {
            $0.flsubcategoria4d0825099c ?? "" < $1.flsubcategoria4d0825099c ?? ""
        }
        subcategoriesCollection.reloadData()
        subcategoriesCollection.reloadInputViews()
    }
}
