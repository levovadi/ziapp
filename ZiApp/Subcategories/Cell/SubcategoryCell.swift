import Foundation
import UIKit
import Kingfisher

class SubCategoryCell: UICollectionViewCell {
    
    static let id = "idSubcategoryCell"
    var currentFavoriteObject: FavoriteResponse?
    var delegateNotif: touchImageFavorite?
    
    var bottomLine: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .lightGray
        return properties
    }()
    
    let imagesubCategory: UIImageView = {
        let properties = UIImageView()
        properties.image = #imageLiteral(resourceName: "placeholderImage")
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var nameSubCategoryLB: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = "española"
        properties.textColor = .black
        properties.sizeToFit()
        properties.font = UIFont.systemFont(ofSize: 17)
        properties.numberOfLines = 0
        properties.textAlignment = .center
        properties.backgroundColor = .clear
        return properties
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        addSubviews()
        setAutolayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(currentId: Int) {
        let subcategoryImage = ApiManager.shared.getDetailSubcategory(idSubcategory: currentId)
        guard let onesubcategoryImage = subcategoryImage.first else { return }
        guard let path = onesubcategoryImage.flimagenpreview1a619ca85d  else { return }
        let generalpath = baseUrl + path
        imagesubCategory.kf.setImage(with: URL(string: generalpath))
        nameSubCategoryLB.text = onesubcategoryImage.flsubcategoria4d0825099c
    }
    
    func addSubviews() {
        self.addSubview(imagesubCategory)
        self.addSubview(nameSubCategoryLB)
        self.addSubview(bottomLine)
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            imagesubCategory.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.70),
            imagesubCategory.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.70),
            imagesubCategory.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0.0),
            imagesubCategory.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: -UIScreen.main.bounds.width * 0.30)
        ])
        
        NSLayoutConstraint.activate([
            nameSubCategoryLB.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            nameSubCategoryLB.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 60.0)
        ])
        
        NSLayoutConstraint.activate([
            bottomLine.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.010),
            bottomLine.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1.20),
            bottomLine.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0),
            bottomLine.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1.0)
        ])
    
    }
    
    
}
