import Foundation
import UIKit
import SafariServices

class HelpScreen: UIViewController {
    
    lazy var scroll: UIScrollView = {
        let config = UIScrollView()
        config.translatesAutoresizingMaskIntoConstraints = false
        config.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        return config
    }()
    
    lazy var contentView: UIView = {
        let properties = UIView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.backgroundColor = .clear
        return properties
    }()
    
    var imageTop: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "img10917")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var saludoText: UILabel = {
        let properties = UILabel()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.text = " Hola! ¿Necesitas ayuda? ¡Danos tus comentarios! "
        properties.numberOfLines = 0
        properties.font = UIFont(name: "Helvetica-bold", size: 15)
        properties.backgroundColor = .clear
        properties.textAlignment = .center
        properties.textColor = .black
        return properties
    }()
    
    var contactButton: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "contactanos")
        properties.contentMode = .scaleAspectFit
        return properties
    }()
    
    var questionsImage: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.image = UIImage(named: "questions")
        properties.contentMode = .scaleToFill
        return properties
    }()
    
    override func viewDidAppear(_ animated: Bool) {}
    override func viewDidDisappear(_ animated: Bool) {}
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        addSubviews()
        setAutolayout()
        addGestureImage()
    }
    
    func addSubviews() {
        self.view.addSubview(scroll)
        self.scroll.addSubview(contactButton)
        self.scroll.addSubview(questionsImage)
        self.scroll.addSubview(contentView)
        contentView.addSubview(imageTop)
        contentView.addSubview(saludoText)
    }
    
    func addGestureImage() {
        self.contactButton.isUserInteractionEnabled = true
        let gestureContact = UITapGestureRecognizer(target: self, action: #selector(openContact(tapGestureRecognizer:)))
        self.contactButton.addGestureRecognizer(gestureContact)
    }
    
    @objc func openContact(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let url = URL(string: "https://wa.me/+524421877724") else { return }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    func setAutolayout() {
        
        NSLayoutConstraint.activate([
            scroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0.0),
            scroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            scroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
            scroll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: UIScreen.main.bounds.height * -0.10)
        ])
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.scroll.topAnchor, constant: 0),
            contentView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.18),
            contentView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.90),
            contentView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            imageTop.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 1),
            imageTop.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 8.0),
            imageTop.trailingAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: -8.0),
            imageTop.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
        ])
        NSLayoutConstraint.activate([
            saludoText.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 1.0),
            saludoText.leadingAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 18.0),
            saludoText.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -18.0),
            saludoText.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8.0)
        ])
        
        NSLayoutConstraint.activate([
            contactButton.topAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 2.0),
            contactButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.80),
            contactButton.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.06),
            contactButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            questionsImage.topAnchor.constraint(equalTo: self.contactButton.bottomAnchor, constant: 8.0),
            questionsImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            questionsImage.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.70),
            questionsImage.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.75),
            questionsImage.bottomAnchor.constraint(equalTo: self.scroll.bottomAnchor, constant: 0)
        ])
        
    }
    
}
