import Foundation
import UIKit

let baseUrl = "http://www.zientrevecinos.com"
//let baseUrl = "http://ziweb6c646c5223.codium.mx"
let currentVersion = "Versión 1.2.6(21)"

let titleAlertError = "Error"
let messageErrorNodataMedia = "Link no cargado en este anuncio."

let primaryColorGreen = UIColor(red: 0/255, green: 178/255, blue: 0/255, alpha: 1.0)
let secondaryColorGreen = UIColor(red: 72/255, green: 104/255, blue: 77/255, alpha: 1.0)
    // UIColor(red: 185/255, green: 216/255, blue: 120/255, alpha: 1.0)
let anothergreen = UIColor(red: 72/255, green: 104/255, blue: 77/255, alpha: 1.0)
    extension UIColor {
        public convenience init?(hex: String) {
            let r, g, b, a: CGFloat

            if hex.hasPrefix("#") {
                let start = hex.index(hex.startIndex, offsetBy: 1)
                let hexColor = String(hex[start...])

                if hexColor.count == 8 {
                    let scanner = Scanner(string: hexColor)
                    var hexNumber: UInt64 = 0

                    if scanner.scanHexInt64(&hexNumber) {
                        r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                        g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                        b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                        a = CGFloat(hexNumber & 0x000000ff) / 255

                        self.init(red: r, green: g, blue: b, alpha: a)
                        return
                    }
                }
            }

            return nil
        }
    }
  

public class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .medium)

    public override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)

        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)

        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
}



