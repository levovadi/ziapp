//
//  ProtocolView.swift
//  ZiApp
//
//  Created by Mariana Méndez on 21/01/21.
//

import Foundation

protocol UIDelegateInterface {
    func initComponents()
    func addComponents()
    func setAutolayout()
}
