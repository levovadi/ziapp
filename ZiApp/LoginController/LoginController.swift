import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import SideMenu
import WalkthroughScroller

protocol optionMenu {
    func changeScreen(option: Int)
}

protocol makeAutoLogin {
    func loginWithUser(usermail: String, pwd: String)
}


class LoginController: UIViewController {
    
    let apiCalls = ApiManager()
    let alertCoordinator = AlertManager()
    var dictionaryLogin: [String:Any] = [:]
  
    var flagIndicator: Bool = true
    
   
    
    lazy var logoImage: UIImageView = {
        let properties = UIImageView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.contentMode = .scaleAspectFit
        properties.image = UIImage(named: "img10929")
        return properties
    }()
    
    var mailInput: MDCFilledTextField = {
        let properties = MDCFilledTextField()
        let whiteColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.tag = 0
        properties.textColor = .blue
        properties.label.highlightedTextColor = UIColor.red
        properties.label.textColor = .white
        properties.label.textAlignment = .center
        properties.label.text = ""
        properties.placeholder = "Correo electrónico"
        properties.attributedPlaceholder = NSAttributedString(string: "Correo electrónico",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        properties.textAlignment = .center
        properties.sizeToFit()
        properties.setUnderlineColor(whiteColor, for: .normal)
        properties.setUnderlineColor(whiteColor, for: .editing)
        properties.setUnderlineColor(whiteColor, for: .disabled)
        properties.setFilledBackgroundColor(.clear, for: .normal)
        properties.setFilledBackgroundColor(.clear, for: .editing)
        properties.setUnderlineColor(whiteColor, for: .editing)
        properties.setTextColor(whiteColor, for: .editing)
        properties.setTextColor(whiteColor, for: .disabled)
        properties.setTextColor(whiteColor, for: .normal)
        properties.textAlignment = .center
        properties.tag = 0
        properties.label.textColor = .white
        properties.setFloatingLabelColor(whiteColor, for: .disabled)
        properties.setFloatingLabelColor(whiteColor, for: .editing)
        properties.setFloatingLabelColor(whiteColor, for: .normal)
        return properties
    }()
    
    var passwordInput: MDCFilledTextField = {
        let properties = MDCFilledTextField()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.tag = 1
        properties.textColor = .blue
        properties.label.text = ""
        properties.placeholder = "Contraseña"
        properties.attributedPlaceholder = NSAttributedString(string: "Contraseña",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        properties.leadingAssistiveLabel.text = "   "
        properties.leadingAssistiveLabel.textAlignment = .center
        properties.sizeToFit()
        properties.setUnderlineColor(.white, for: .normal)
        properties.setUnderlineColor(.white, for: .editing)
        properties.setUnderlineColor(.white, for: .disabled)
        properties.isSecureTextEntry = true
        properties.trailingView?.isHidden = false
        properties.trailingView = UIImageView(image: UIImage(named: "eye24"))
        properties.trailingViewMode = .always
        let gesture = UIGestureRecognizer(target: self, action: #selector(eyeIconAction(tapGestureRecognizer:)))
        properties.isUserInteractionEnabled = true
        properties.trailingView?.addGestureRecognizer(gesture)
        properties.tag = 1
        properties.addTarget(self, action: #selector(textfieldClicked), for: .touchDown)
        properties.setFilledBackgroundColor(.clear, for: .normal)
        properties.setFilledBackgroundColor(.clear, for: .editing)
        properties.setTextColor(.white, for: .editing)
        properties.setTextColor(.white, for: .disabled)
        properties.setTextColor(.white, for: .normal)
        properties.textAlignment = .center
        properties.setFloatingLabelColor(.white, for: .disabled)
        properties.setFloatingLabelColor(.white, for: .editing)
        properties.setFloatingLabelColor(.white, for: .normal)
        properties.label.textAlignment = .center
        
        return properties
    }()
    
    let button: MDCButton = {
        let properties = MDCButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        properties.setTitle("ENTRAR", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.inkColor = UIColor.green
        properties.tintColor = UIColor.systemGreen
        properties.rippleColor = primaryColorGreen
        return properties
    }()
    
    let invitedButton: MDCButton = {
        let properties = MDCButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        properties.setTitle("INVITADO", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.inkColor = UIColor.systemGreen
        properties.tintColor = UIColor.darkGray
        properties.rippleColor = primaryColorGreen
        properties.backgroundColor = secondaryColorGreen
        return properties
    }()
    
    let button1: MDCButton = {
        let properties = MDCButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        properties.setTitle("REGISTRATE", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.inkColor = UIColor.systemGreen
        properties.tintColor = UIColor.darkGray
        properties.backgroundColor = secondaryColorGreen
        properties.rippleColor = primaryColorGreen
        return properties
    }()
    
    let button2: MDCButton = {
        let properties = MDCButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        properties.setTitle("¿QUIERES ANUNCIAR?", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.inkColor = UIColor.systemGreen
        properties.tintColor = UIColor.darkGray
        properties.rippleColor = primaryColorGreen
        properties.backgroundColor = secondaryColorGreen
        return properties
    }()
    
    let button3: MDCButton = {
        let properties = MDCButton()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.setElevation(ShadowElevation(rawValue: 1.0), for: .normal)
        properties.setTitle("OLVIDE LA CONTRASEÑA", for: .normal)
        properties.backgroundColor = primaryColorGreen
        properties.inkColor = UIColor.systemGreen
        properties.tintColor = UIColor.darkGray
        properties.rippleColor = primaryColorGreen
        properties.backgroundColor = secondaryColorGreen
        return properties
    }()
    
    var stackButtons: UIStackView = {
        let properties = UIStackView()
        properties.translatesAutoresizingMaskIntoConstraints = false
        properties.axis = .vertical
        properties.distribution = .fillEqually
        properties.backgroundColor = .clear
        properties.spacing = 12.0
        return properties
    }()
    
    // MARK: this is for save or dont lose current session
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.alpha = 0
        let currentUser = DataManager.shared.getDataUser()
        if currentUser.flcorreoelectronico57997cc6aa != nil {
            let resultadoLogin = ApiManager.shared.loginWs(usermail: currentUser.flcorreoelectronico57997cc6aa ?? "", password: currentUser.flcontrasenaa8f7f67349 ?? "",isLogged: true)
            if resultadoLogin.count > 0 {
                self.mailInput.text = ""
                self.passwordInput.text = ""
                if !DataManager.shared.isInvited().0 {
                    self.navigationController?.pushViewController(HomeCategory(), animated: true)
                } else {
                    self.view.alpha = 1
                }
            } else {
                self.view.alpha = 1
                self.present(self.alertCoordinator.defaultalert(title: "No encontrado", message: "Usuario no encontrado"), animated: true, completion: nil)
            }
        } else {
            // MARK: cuando cierra sesión entra aquí
            self.view.alpha = 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "backgroundLogin"))
        addSubviews()
        stackConfiguration()
        setAutolayout()
        setNavItems()
        configureLateralMenu()
        addTargetButton()
        addDelegatesTextfields()
        dismissKeyboardsGesture()
    }
    
    
    func addTargetButton() {
        button.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        button1.addTarget(self, action: #selector(registerAction), for: .touchUpInside)
        button2.addTarget(self, action: #selector(moreinformationAction), for: .touchUpInside)
        button3.addTarget(self, action: #selector(forgetpasswordAction), for: .touchUpInside)
        invitedButton.addTarget(self, action: #selector(toggleInvited), for: .touchUpInside)
    }
    
    @objc func toggleInvited() {
        let result = self.apiCalls.loginWs(usermail: "oscar.fandino@palmersa.com", password: "123")
        if result.count > 0 {
            DataManager.shared.setFlagInvited(value: true, isFirstTimeInvited: true)
            self.mailInput.text = ""
            self.passwordInput.text = ""
            let zoneChooseController = PerfilViewController()
            zoneChooseController.flag = .myProfile
            self.navigationController?.pushViewController(zoneChooseController, animated: true)
        } else {
            DataManager.shared.setFlagInvited(value: false, isFirstTimeInvited: false)
            self.present(self.alertCoordinator.defaultalert(title: "No encontrado", message: "Usuario no encontrado"), animated: true, completion: nil)
        }
    }
    
    @objc func loginAction() {
        self.view.endEditing(true)
        createSpinnerView()
        guard let mail = dictionaryLogin["usermail"] as? String else { return }
        guard let pass = dictionaryLogin["password"] as? String else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            let result = self.apiCalls.loginWs(usermail: mail, password: pass)
            if result.count > 0 {
                self.mailInput.text = ""
                self.passwordInput.text = ""
                DataManager.shared.setFlagInvited(value: false, isFirstTimeInvited: false)
                self.navigationController?.pushViewController(HomeCategory(), animated: true)
            } else {
                self.present(self.alertCoordinator.defaultalert(title: "No encontrado", message: "Usuario no encontrado"), animated: true, completion: nil)
            }
        })
    }
    
    func createSpinnerView() {
        
        let child = SpinnerViewController()
        
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            child.willMove(toParent: nil)
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
        
    }
    
    @objc func registerAction() {
        self.view.endEditing(true)
        let profileRegister = PerfilViewController()
        profileRegister.flag = .register
        profileRegister.delegate = self
        self.navigationController?.present(profileRegister, animated: true, completion: nil)
    }
    
    @objc func moreinformationAction() {
        self.view.endEditing(true)
        let screen = RegistrationWebController()
        self.navigationController?.present(screen, animated: true, completion: nil)
    }
    
    @objc func forgetpasswordAction() {
        self.view.endEditing(true)
        let nv = RegistrationWebController()
        nv.urlString = "http://6c646c5ab7a94fbfa6d3729edb265223.codium.mx/Recuperarcontrasenausuario"
        self.navigationController?.present(nv, animated: true, completion: nil)
    }
    
    func addSubviews() {
        self.view.addSubview(logoImage)
        self.view.addSubview(mailInput)
        self.view.addSubview(passwordInput)
        self.view.addSubview(stackButtons)
    }
    
    func stackConfiguration() {
        stackButtons.addArrangedSubview(button)
        stackButtons.addArrangedSubview(button1)
        stackButtons.addArrangedSubview(invitedButton)
        stackButtons.addArrangedSubview(button2)
        stackButtons.addArrangedSubview(button3)
    }
    
    func setAutolayout() {
        NSLayoutConstraint.activate([
            logoImage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 8.0),
            logoImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            logoImage.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.20),
            logoImage.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15)
        ])
        
        NSLayoutConstraint.activate([
            mailInput.topAnchor.constraint(equalTo: self.logoImage.bottomAnchor, constant: 8.0),
            mailInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            mailInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
        ])
        
        NSLayoutConstraint.activate([
            passwordInput.topAnchor.constraint(equalTo: self.mailInput.bottomAnchor, constant: 8.0),
            passwordInput.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0),
            passwordInput.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0),
        ])
        
        NSLayoutConstraint.activate([
            stackButtons.topAnchor.constraint(equalTo: self.passwordInput.bottomAnchor, constant: 8.0),
            stackButtons.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8.0),
            stackButtons.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8.0),
            stackButtons.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -UIScreen.main.bounds.width * 0.30)
        ])
        
        
    }
    
    private func dismissKeyboardsGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func setNavItems() {
        self.navigationController?.navigationBar.barTintColor = primaryColorGreen
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    @objc func menuPressed() {
        lateralMenu()
    }
    
    @objc func searchAction() {
        
    }
    
    @objc func eyeIconAction(tapGestureRecognizer: UITapGestureRecognizer) {
        
    }
    
    @objc func textfieldClicked(textField: UITextField) {
        if flagIndicator {
            passwordInput.isSecureTextEntry = true
            passwordInput.trailingView = UIImageView(image: UIImage(named: "eye24"))
        } else {
            passwordInput.isSecureTextEntry = false
            passwordInput.trailingView = UIImageView(image: UIImage(named: "eyeHide"))
        }
        flagIndicator = !flagIndicator
    }
    
    func lateralMenu() {
        let controllerMenu = MenuController()
        controllerMenu.delegateOptionMenu = self
        let menu = SideMenuNavigationController(rootViewController: controllerMenu)
        menu.leftSide = true
        menu.statusBarEndAlpha = 0
        self.present(menu, animated: true, completion: nil)
    }
    
    func configureLateralMenu() {
        
    }
    
}

extension LoginController: UITextFieldDelegate {
    
    func addDelegatesTextfields() {
        self.mailInput.delegate = self
        self.passwordInput.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            dictionaryLogin["usermail"] = textField.text
        case 1:
            dictionaryLogin["password"] = textField.text
        default:
            print("not mapped textfield")
        }
    }
    
}

extension LoginController: makeAutoLogin {
    
    func loginWithUser(usermail: String, pwd: String) {
        let result = self.apiCalls.loginWs(usermail: usermail, password: pwd,isLogged: true)
        if result.count > 0 {
            self.mailInput.text = ""
            self.passwordInput.text = ""
            self.navigationController?.pushViewController(HomeCategory(), animated: true)
        } else {
            self.present(self.alertCoordinator.defaultalert(title: "No encontrado", message: "Usuario no encontrado"), animated: true, completion: nil)
        }
    }
    
}

extension LoginController: optionMenu {
    func changeScreen(option: Int) {
        switch option {
        case 0:
            self.navigationController?.pushViewController(PerfilViewController(), animated: true)
        case 1:
            self.navigationController?.pushViewController(HomeCategory(), animated: true)
        case 2:
            self.navigationController?.pushViewController(FavoritesController(), animated: true)
        case 3:
            self.navigationController?.pushViewController(RegistrationWebController(), animated: true)
        case 4:
            DataManager.shared.closeLogin(ifCloseSession: true)
            DataManager.shared.removeUser()
            self.navigationController?.popToRootViewController(animated: true)
        default:
            self.dismiss(animated: true, completion: nil)
        }
    }
}




extension Sequence {
    func splitBefore(
        separator isSeparator: (Iterator.Element) throws -> Bool
    ) rethrows -> [AnySequence<Iterator.Element>] {
        var result: [AnySequence<Iterator.Element>] = []
        var subSequence: [Iterator.Element] = []

        var iterator = self.makeIterator()
        while let element = iterator.next() {
            if try isSeparator(element) {
                if !subSequence.isEmpty {
                    result.append(AnySequence(subSequence))
                }
                subSequence = [element]
            }
            else {
                subSequence.append(element)
            }
        }
        result.append(AnySequence(subSequence))
        return result
    }
}

extension Character {
    var isUpperCase: Bool { return String(self) == String(self).uppercased() }
}

extension LoginController: WalkthorughScrollerDelegate {
    
    func ctaBtnDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
