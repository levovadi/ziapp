

import Foundation
import UIKit

enum keys: String {
    case contrasenia
    case correo
    case identificador
    case zona
}


class DataManager {
    let defaults = UserDefaults.standard
    public static let shared = DataManager()
    
    func getFlagCloseLogin() -> Bool {
        defaults.bool(forKey: "ifCloseSession")
    }
    
    func closeLogin(ifCloseSession: Bool) {
        defaults.setValue(ifCloseSession, forKey: "ifCloseSession")
    }
    
    func firstEntry(isFirstEntry: Bool) {
        defaults.setValue(isFirstEntry, forKey: "isFirstEntry")
    }
    
    func getFlagFirstEntry() -> Bool {
        return defaults.bool(forKey: "isFirstEntry")
    }
    
    func saveCurrentCategory(idCat: Int) {
        defaults.setValue(idCat, forKey: "currentCatId")
    }
    
    func loadCurrentCategory() -> Int {
        return defaults.integer(forKey: "currentCatId")
    }
    
    func setFlagInvited(value: Bool, isFirstTimeInvited: Bool) {
        defaults.setValue(value, forKey: "isInvited")
        defaults.setValue(isFirstTimeInvited, forKey: "isFirstTimeInvited")
    }
    
    func isInvited() -> (Bool,Bool) {
        let isInvited = defaults.bool(forKey: "isInvited")
        let isFirstTimeInvited = defaults.bool(forKey: "isFirstTimeInvited")
        return (isInvited,isFirstTimeInvited)
    }
    
    func saveUserData(userData: LoginResponse) {
        defaults.setValue(userData.flcontrasenaa8f7f67349, forKey: keys.contrasenia.rawValue)
        defaults.setValue(userData.flcorreoelectronico57997cc6aa, forKey: keys.correo.rawValue)
        defaults.setValue(userData.flidentificadore7dbf413f9, forKey: keys.identificador.rawValue)
        defaults.setValue(userData.flzona621bfe9801, forKey: keys.zona.rawValue)
    }
    
    func getDataUser() -> LoginResponse {
        let password = defaults.string(forKey: keys.contrasenia.rawValue)
        let correo = defaults.string(forKey: keys.correo.rawValue)
        let identificador = defaults.integer(forKey: keys.identificador.rawValue)
        let zona = defaults.integer(forKey: keys.zona.rawValue)
        return LoginResponse(flcontrasenaa8f7f67349: password, flcorreoelectronico57997cc6aa: correo, flidentificadore7dbf413f9: identificador, flzona621bfe9801: zona)
    }
    
    func removeUser() {
        defaults.removeObject(forKey: keys.contrasenia.rawValue)
        defaults.removeObject(forKey: keys.correo.rawValue)
        defaults.removeObject(forKey: keys.identificador.rawValue)
        defaults.removeObject(forKey: keys.zona.rawValue)
    }
    
}

