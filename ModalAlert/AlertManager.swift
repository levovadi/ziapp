//
//  AlertManager.swift
//  ZiApp
//
//  Created by Mariana Méndez on 27/01/21.
//

import Foundation
import UIKit

class AlertManager {
    
    public static let shared = AlertManager()
    
    func defaultalert(title: String, message: String ) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
    
    
}
